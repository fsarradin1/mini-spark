# Mini-spark

Project made for education purpose.

## Auto-Serde

The used serde for protocol is based on Magnolia to simplify the
mapping between the binary protocol and an object model. Thus, there
is no intermediate representation to analyse.

## Tests

### Test0: JITC

Using Janino for runtime compilation of Java code in a string.

### Test1: Selector

Tackling difficulties behind the channel selector.

### Test2: Serde v1

Use the Kafka way to create a communication protocol including byte
array to intermediate representation mapper.

### Test3: Use auto-serde in communication

Establish a client-server communication using auto-serde.

### Test4: Lambda serialization

### Test5: gRPC

### Test6: cluster management and remote computation with actors

### Test7: example with remote actors