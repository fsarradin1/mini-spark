package io.univalence.mini_spark.test.test8_partition

import java.io.ByteArrayInputStream
import org.scalatest.funsuite.AnyFunSuiteLike
import io.univalence.mini_spark.rdd._

class SplitFileReaderIteratorTest extends AnyFunSuiteLike {

  test("should read a split of one line at start") {
    val in   = new ByteArrayInputStream("hello\nworld".getBytes())
    val data = new SplitFileReaderIterator(in, 0, 4).toList

    assert(data == List("hello"))
  }

  test("should read a split of one line at later split") {
    val in   = new ByteArrayInputStream("hello\nworld".getBytes())
    val data = new SplitFileReaderIterator(in, 3, 4).toList

    assert(data == List("world"))
  }

  test("should read a split of two lines at start") {
    val in = new ByteArrayInputStream(
      "hello1\nhello2\nhello3\nhello4\nhello5\nhello6".getBytes()
    )
    val data = new SplitFileReaderIterator(in, 0, 12).toList

    assert(data == List("hello1", "hello2"))
  }

  test("should read a split of two lines at later split") {
    val in = new ByteArrayInputStream(
      "hello1\nhello2\nhello3\nhello4\nhello5\nhello6".getBytes()
    )
    val data = new SplitFileReaderIterator(in, 13, 12).toList

    assert(data == List("hello3", "hello4"))
  }

  test("should read a split of two lines at close to end") {
    val in = new ByteArrayInputStream(
      "hello1\nhello2\nhello3\nhello4\nhello5\nhello6".getBytes()
    )
    val data = new SplitFileReaderIterator(in, 26, 12).toList

    assert(data == List("hello5", "hello6"))
  }

  test("should not read additional lines") {
    val in   = new ByteArrayInputStream("hello1\nhello2".getBytes())
    val data = new SplitFileReaderIterator(in, 0, 13).toList

    println(data)
    assert(data == List("hello1", "hello2"))
  }

}
