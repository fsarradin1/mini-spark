package io.univalence.mini_spark.test.test3_serde_net

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import org.scalatest.funsuite.AnyFunSuiteLike
import io.univalence.mini_spark.test.test3_serde_net.EndPoint._

class SerdeTest extends AnyFunSuiteLike {
  import serde._

  test("should serde byte") {
    val buffer = ByteBuffer.allocate(Serde[Byte].sizeOf(0))
    42.toByte.writeTo(buffer).get
    buffer.rewind()
    val result = Serde[Byte].readFrom(buffer).get

    assert(result === 42)
  }

  test("should serde short") {
    val buffer = ByteBuffer.allocate(Serde[Short].sizeOf(0))
    4242.toShort.writeTo(buffer).get
    buffer.rewind()
    val result = Serde[Short].readFrom(buffer).get

    assert(result === 4242)
  }

  test("should serde int") {
    val buffer = ByteBuffer.allocate(Serde[Int].sizeOf(0))
    42424242.writeTo(buffer).get
    buffer.rewind()
    val result = Serde[Int].readFrom(buffer).get

    assert(result === 42424242)
  }

  test("should serde long") {
    val buffer = ByteBuffer.allocate(Serde[Long].sizeOf(0))
    42424242L.writeTo(buffer).get
    buffer.rewind()
    val result = Serde[Long].readFrom(buffer).get

    assert(result === 42424242L)
  }

  test("should serde string") {
    val content = "hello world!"
    val buffer  = ByteBuffer.allocate(Serde[String].sizeOf(content))
    content.writeTo(buffer).get
    buffer.rewind()
    val result = Serde[String].readFrom(buffer).get

    assert(result === "hello world!")
  }

  test("should serde byte array") {
    val content = "hello world!".getBytes(StandardCharsets.UTF_8)
    val buffer  = ByteBuffer.allocate(Serde[Array[Byte]].sizeOf(content))
    content.writeTo(buffer).get
    buffer.rewind()
    val result = new String(Serde[Array[Byte]].readFrom(buffer).get)

    assert(result === "hello world!")
  }

  test("should serde simple structure with mapping") {
    val simpleSerde = new SerdeDerivation(Map(
      0x00.toShort -> "Simple"
    ))

    val content = Simple(42, "John")
    val buffer  = ByteBuffer.allocate(Serde[Simple].sizeOf(content))
    content.writeTo(buffer).get
    buffer.rewind()
    val result = Serde[Simple].readFrom(buffer).get

    assert(result === Simple(42, "John"))
  }

  test("should serde simple structure without mapping") {
    val simpleSerde = new SerdeDerivation(Map())

    val content = Simple(42, "John")
    val buffer  = ByteBuffer.allocate(Serde[Simple].sizeOf(content))
    content.writeTo(buffer).get
    buffer.rewind()
    val result = Serde[Simple].readFrom(buffer).get

    assert(result === Simple(42, "John"))
  }

  test("should serde ADT") {
    val simpleSerde = new SerdeDerivation(
      Map[Short, String](
        0x2f.toShort -> "Case1",
        0x01.toShort -> "Case2"
      )
    )

    val content = Root.Case1(42)
    val buffer  = ByteBuffer.allocate(Serde[Root].sizeOf(content))
    content.writeTo(buffer).get
    buffer.rewind()
    val result = Serde[Root].readFrom(buffer).get

    assert(result === Root.Case1(42))
  }
}

case class Simple(field1: Int, field2: String)

sealed trait Root
object Root {
  case class Case1(field: Int)         extends Root
  case class Case2(otherField: String) extends Root
}
