package io.univalence.mini_spark.test.test3_serde_net

import java.nio.ByteBuffer
import org.scalatest.funsuite.AnyFunSuiteLike
import io.univalence.mini_spark.test._
import io.univalence.mini_spark.test.test3_serde_net.EndPoint._

class MessageTest extends AnyFunSuiteLike {
  import serde._

  test("should serde a message in a channel") {
    val channel = new MockChannel()
    val message = Message.from(Echo.EchoRequest("hello"), 0).get
    message.writeTo(channel).get
    channel.flip()
    val result = Message.readFrom[Echo.EchoRequest](channel).get

    assert(result === message)
  }

  test("should not serialize a message due to bad magic number") {
    val channel = new MockChannel()
    val request = Echo.EchoRequest("hello")
    val message =
      Message(
        Header(0x0000.toShort, 0, 0, request.byteSize),
        request
      )
    val result = message.writeTo(channel)

    assert(result.isFailure)
  }

  test("should not serialize a message due to bad type ID") {
    val channel = new MockChannel()
    val request = Echo.EchoRequest("hello")
    val message =
      Message(
        Header(Message.magic, 0, 0xffff.toShort, request.byteSize),
        request
      )
    val result = message.writeTo(channel)

    assert(result.isFailure)
  }

  test("should not deserialize a message due to bad magic number") {
    val channel = new MockChannel()
    val request = Echo.EchoRequest("hello")
    val header = Header(0x0000.toShort, 0, 0, request.byteSize)

    val headerBuffer = ByteBuffer.allocate(header.byteSize)
    header.writeTo(headerBuffer)
    headerBuffer.flip()
    channel.write(headerBuffer)
    val buffer = ByteBuffer.allocate(request.byteSize)
    request.writeTo(buffer)
    channel.write(buffer)

    channel.flip()
    val result = Message.readFrom[Echo.EchoRequest](channel)

    assert(result.isFailure)
  }

  test("should not deserialize a message due to bad type ID") {
    val channel = new MockChannel()
    val request = Echo.EchoRequest("hello")
    val header = Header(Message.magic, 0, 0xffff.toShort, request.byteSize)

    val headerBuffer = ByteBuffer.allocate(header.byteSize)
    header.writeTo(headerBuffer)
    headerBuffer.flip()
    channel.write(headerBuffer)
    val buffer = ByteBuffer.allocate(request.byteSize)
    request.writeTo(buffer)
    channel.write(buffer)

    channel.flip()
    val result = Message.readFrom[Echo.EchoRequest](channel)

    assert(result.isFailure)
  }
}
