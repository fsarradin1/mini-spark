package io.univalence.mini_spark.test.test2_imp_types

import io.univalence.mini_spark.test.test2_imp_types.IOType._
import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import org.scalacheck.{Arbitrary, Gen, Prop}
import org.scalatest.funsuite.AnyFunSuiteLike
import org.scalatestplus.scalacheck.Checkers
import scala.collection.immutable.ListMap
import scala.util.Success

class IOTypeTest extends AnyFunSuiteLike with Checkers {
  test("IOInt8 should do serde") {
    val buffer = ByteBuffer.allocate(IOInt8.size)
    IOInt8.write(buffer, 42)
    buffer.rewind()

    assert(IOInt8.read(buffer) === Success(42))
  }

  test("IOInt16 should do serde") {
    val buffer = ByteBuffer.allocate(IOInt16.size)
    IOInt16.write(buffer, 4242)
    buffer.rewind()

    assert(IOInt16.read(buffer) === Success(4242))
  }

  test("IOInt32 should do serde") {
    val buffer = ByteBuffer.allocate(IOInt32.size)
    IOInt32.write(buffer, 42424242)
    buffer.rewind()

    assert(IOInt32.read(buffer) === Success(42424242))
  }

  test("IOInt64 should do serde") {
    val buffer = ByteBuffer.allocate(IOInt64.size)
    IOInt64.write(buffer, 4242424242424242L)
    buffer.rewind()

    assert(IOInt64.read(buffer) === Success(4242424242424242L))
  }

  test("IOString should do serde") {
    val content = "hello world!"
    val buffer  = ByteBuffer.allocate(IOString.sizeOf(content))
    IOString.write(buffer, content)
    buffer.rewind()

    assert(IOString.read(buffer) === Success("hello world!"))
  }

  test("IOByteArray should do serde") {
    val content = "hello world!".getBytes(StandardCharsets.UTF_8)
    val buffer  = ByteBuffer.allocate(IOByteArray.sizeOf(content))
    IOByteArray.write(buffer, content)
    buffer.rewind()

    assert(
      IOByteArray.read(buffer).map(ab => new String(ab)) === Success(
        "hello world!"
      )
    )
  }

  test("IOArray of String should do serde") {
    val IOStringArray = new IOArray(IOString)
    val content       = Array("hello", "world")
    val buffer        = ByteBuffer.allocate(IOStringArray.sizeOf(content))
    IOStringArray.write(buffer, content)
    buffer.rewind()

    assert(
      IOStringArray.read(buffer).map(_.toList) === Success(
        List("hello", "world")
      )
    )
  }

  test("IOSchema with one field should do serde") {
    val schema = IOSchema(
      IOSchema.String("message")
    )
    val structure = Structure(schema, Seq("hello world!"))

    val buffer = ByteBuffer.allocate(schema.sizeOf(structure))
    schema.write(buffer, structure)
    buffer.rewind()
    val result = schema.read(buffer).get

    assert(
      ListMap.from(result.values) === Map(
        "message" -> "hello world!"
      )
    )
  }

  test("IOSchema with three fields should do serde") {
    val schema = IOSchema(
      IOSchema.Int16("id"),
      IOSchema.String("name"),
      IOSchema.Int8("age")
    )
    val structure = Structure(schema, Seq(123.toShort, "John", 32.toByte))

    val buffer = ByteBuffer.allocate(schema.sizeOf(structure))
    schema.write(buffer, structure)
    buffer.rewind()
    val result = schema.read(buffer).get

    assert(
      ListMap.from(result.values) === ListMap(
        "id"   -> 123,
        "name" -> "John",
        "age"  -> 32
      )
    )
  }

  test("IOSchema with recursive field should do serde") {
    val addressSchema = IOSchema(
      IOSchema.String("street"),
      IOSchema.String("city")
    )
    val schema = IOSchema(
      IOSchema.Int16("id"),
      IOSchema.String("name"),
      IOSchema.Structure("address", addressSchema)
    )
    val structure = Structure(
      schema,
      Seq(
        123.toShort,
        "John",
        Structure(addressSchema, Seq("Happy Street", "Happyville"))
      )
    )

    val buffer = ByteBuffer.allocate(schema.sizeOf(structure))
    schema.write(buffer, structure)
    buffer.rewind()
    val result =
      schema
        .read(buffer)
        .get
        .values
        .view
        .mapValues {
          case s: Structure => s.values.toMap
          case f            => f
        }

    assert(
      ListMap.from(result) === ListMap(
        "id"   -> 123,
        "name" -> "John",
        "address" -> ListMap(
          "street" -> "Happy Street",
          "city"   -> "Happyville"
        )
      )
    )
  }

  test("property: IOInt8 write/read symmetry") {
    checkWriteReadSymmetry(IOInt8)
  }

  test("property: IOInt16 write/read symmetry") {
    checkWriteReadSymmetry(IOInt16)
  }

  test("property: IOInt32 write/read symmetry") {
    checkWriteReadSymmetry(IOInt32)
  }

  test("property: IOInt64 write/read symmetry") {
    checkWriteReadSymmetry(IOInt64)
  }

  test("property: IOString write/read symmetry") {
    checkWriteReadSymmetry(IOString)
  }

  test("property: IOByteArray write/read symmetry") {
    checkWriteReadSymmetry(IOByteArray)
  }

  test("property: IOArray of Int write/read symmetry") {
    checkWriteReadSymmetry(new IOArray(IOInt32))
  }

  test("property: IOSchema of Int and String write/read symmetry") {
    val schema = IOSchema(IOSchema.Int32("id"), IOSchema.String("name"))

    implicit val structures: Arbitrary[Structure] = Arbitrary {
      for {
        id   <- Gen.choose[Int](0, 1000)
        name <- Gen.asciiStr
      } yield Structure(schema, Seq(id, name))
    }

    checkWriteReadSymmetry(schema)
  }

  def checkWriteReadSymmetry[A: Arbitrary](ioType: IOType[A]): Prop = {
    Prop.forAll { value: A =>
      val buffer: ByteBuffer = ByteBuffer.allocate(ioType.sizeOf(value))
      ioType.write(buffer, value)
      buffer.rewind()
      value === ioType.read(buffer)
    }
  }
}
