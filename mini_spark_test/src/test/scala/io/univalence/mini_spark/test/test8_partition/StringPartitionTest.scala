package io.univalence.mini_spark.test.test8_partition

import org.scalatest.funsuite.AnyFunSuiteLike
import io.univalence.mini_spark.rdd._

class StringPartitionTest extends AnyFunSuiteLike {

  test("should partition string in 2") {
    val data = "1234567890"
    val paritions = StringPartition.getPartitions(data, 2)

    assert(paritions == List(
      StringPartition(data, "0", 0, 5),
      StringPartition(data, "1", 5, 5)
    ))
  }

  test("should partition string in 3") {
    val data = "1234567890"
    val paritions = StringPartition.getPartitions(data, 3)

    assert(paritions == List(
      StringPartition(data, "0", 0, 4),
      StringPartition(data, "1", 4, 4),
      StringPartition(data, "2", 8, 2),
    ))
  }

  test("should partition string in 4") {
    val data = "1234567890"
    val paritions = StringPartition.getPartitions(data, 4)

    assert(paritions == List(
      StringPartition(data, "0", 0, 3),
      StringPartition(data, "1", 3, 3),
      StringPartition(data, "2", 6, 3),
      StringPartition(data, "3", 9, 1),
    ))
  }

  test("should partition string in 5") {
    val data = "1234567890"
    val paritions = StringPartition.getPartitions(data, 5)

    assert(paritions == List(
      StringPartition(data, "0", 0, 2),
      StringPartition(data, "1", 2, 2),
      StringPartition(data, "2", 4, 2),
      StringPartition(data, "3", 6, 2),
      StringPartition(data, "4", 8, 2),
    ))
  }

  test("should partition string in 6") {
    val data = "1234567890"
    val paritions = StringPartition.getPartitions(data, 6)

    assert(paritions == List(
      StringPartition(data, "0", 0, 2),
      StringPartition(data, "1", 2, 2),
      StringPartition(data, "2", 4, 2),
      StringPartition(data, "3", 6, 2),
      StringPartition(data, "4", 8, 2)
    ))
  }

}
