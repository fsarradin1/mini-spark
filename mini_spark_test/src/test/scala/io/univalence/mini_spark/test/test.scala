package io.univalence.mini_spark

import java.nio.ByteBuffer
import java.nio.channels.ByteChannel

package object test {
  class MockChannel(capacity: Int = 1024) extends ByteChannel {
    val buffer: ByteBuffer = ByteBuffer.allocate(capacity)
    var _isOpen: Boolean   = true

    override def write(src: ByteBuffer): Int = {
      val start = buffer.position()
      buffer.put(src)

      buffer.position() - start
    }

    override def read(dst: ByteBuffer): Int = {
      val start = buffer.position()
      val array = Array.ofDim[Byte](dst.capacity())
      buffer.get(array)
      dst.put(array)

      buffer.position() - start
    }

    override def isOpen: Boolean = _isOpen

    override def close(): Unit = _isOpen = false

    def rewind(): Unit = buffer.rewind()
    def flip(): Unit   = buffer.flip()
  }
}
