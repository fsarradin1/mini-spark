package io.univalence.mini_spark.test.test8_partition

import java.io.ByteArrayInputStream
import org.scalatest.funsuite.AnyFunSuiteLike
import io.univalence.mini_spark.rdd._

class TextFileReaderTest extends AnyFunSuiteLike {

  test("should notice end of line on end of file") {
    assert(TextFileReader.hasEndOfLine("".getBytes(), 0, -1))
  }

  test("should notice end of line on empty line") {
    assert(TextFileReader.hasEndOfLine("\n".getBytes(), 0, 4))
  }

  test("should not notice end of line on non-empty line") {
    assert(!TextFileReader.hasEndOfLine("abc\nabc".getBytes(), 0, 7))
  }

  test("should read empty file") {
    val in = new ByteArrayInputStream("".getBytes())
    val reader = new TextFileReader(in, 1024)

    val (n, data) = reader.readLine()

    assert(n == -1)
    assert(data == "")
  }

  test("should read one line") {
    val in = new ByteArrayInputStream("hello".getBytes())
    val reader = new TextFileReader(in, 1024)

    val (n, data) = reader.readLine()

    assert(n == 5)
    assert(data == "hello")
  }

  test("should read first line only") {
    val in = new ByteArrayInputStream("hello\nworld".getBytes())
    val reader = new TextFileReader(in, 1024)

    val (n, data) = reader.readLine()

    assert(n == 6)
    assert(data == "hello")
  }

  test("should read second line only") {
    val in = new ByteArrayInputStream("hello\nworld".getBytes())
    val reader = new TextFileReader(in, 1024)

    val (n1, data1) = reader.readLine()
    val (n, data) = reader.readLine()

    assert(n1 == 6)
    assert(data1 == "hello")
    assert(n == 5)
    assert(data == "world")
  }

  test("should read first line with short buffer") {
    val in = new ByteArrayInputStream("hello".getBytes())
    val reader = new TextFileReader(in, 4)

    val (n, data) = reader.readLine()

    assert(n == 5)
    assert(data == "hello")
  }

  test("should read two lines with short buffer") {
    val in = new ByteArrayInputStream("hello\nworld".getBytes())
    val reader = new TextFileReader(in, 4)

    val (n1, data1) = reader.readLine()
    val (n, data) = reader.readLine()

    assert(n1 == 6)
    assert(data1 == "hello")
    assert(n == 5)
    assert(data == "world")
  }

}
