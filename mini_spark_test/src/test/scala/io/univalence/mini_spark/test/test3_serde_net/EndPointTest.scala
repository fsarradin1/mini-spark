package io.univalence.mini_spark.test.test3_serde_net

import io.univalence.mini_spark.test.test3_serde_net.EndPoint._
import java.nio.ByteBuffer
import org.scalatest.funsuite.AnyFunSuiteLike

class EndPointTest extends AnyFunSuiteLike {
  import serde._

  test("should serde a request alone") {
    val request = Echo.EchoRequest("hello")
    val buffer  = ByteBuffer.allocate(request.byteSize)
    request.writeTo(buffer)
    buffer.rewind()
    val result = Serde[Echo.EchoRequest].readFrom(buffer).get

    assert(result === request)
  }

  test("should serde a header alone") {
    val header = Header(0xabcd.toShort, 0x12345678, 0x0000.toShort, 0x42)
    val buffer = ByteBuffer.allocate(Header.byteSize)
    header.writeTo(buffer).get
    buffer.rewind()
    val result = Serde[Header].readFrom(buffer).get

    assert(result === header)
  }

}
