package io.univalence.mini_spark.test.test2_imp_types

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import scala.collection.mutable
import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}

sealed trait IOType[A] {
  type Data = A

  def read(buffer: ByteBuffer): Try[A]

  def write(buffer: ByteBuffer, data: A): Try[Unit]

  def sizeOf(data: A): Int
}

object IOType {
  sealed trait FixedSizeIOType[A] extends IOType[A] {
    def size: Int
  }

  sealed trait VariableSizeIOType[A] extends IOType[A] {
    def sizeOf(data: A): Int
  }

  case object IOInt8 extends FixedSizeIOType[Byte] {
    override def read(buffer: ByteBuffer): Try[Byte] =
      Try {
        buffer.get()
      }

    override def write(buffer: ByteBuffer, data: Byte): Try[Unit] =
      Try {
        buffer.put(data)
      }

    override def size: Int = 1

    override def sizeOf(data: Byte): Int = size
  }

  case object IOInt16 extends FixedSizeIOType[Short] {
    override def read(buffer: ByteBuffer): Try[Short] =
      Try {
        buffer.getShort()
      }

    override def write(buffer: ByteBuffer, data: Short): Try[Unit] =
      Try {
        buffer.putShort(data)
      }

    override def size: Int = 2

    override def sizeOf(data: Short): Int = size
  }

  case object IOInt32 extends FixedSizeIOType[Int] {
    override def read(buffer: ByteBuffer): Try[Int] =
      Try {
        buffer.getInt()
      }

    override def write(buffer: ByteBuffer, data: Int): Try[Unit] =
      Try {
        buffer.putInt(data)
      }

    override def size: Int = 4

    override def sizeOf(data: Int): Int = size
  }

  case object IOInt64 extends FixedSizeIOType[Long] {
    override def read(buffer: ByteBuffer): Try[Long] =
      Try {
        buffer.getLong()
      }

    override def write(buffer: ByteBuffer, data: Long): Try[Unit] =
      Try {
        buffer.putLong(data)
      }

    override def size: Int = 8

    override def sizeOf(data: Long): Int = size
  }

  case object IOByteArray extends VariableSizeIOType[Array[Byte]] {
    override def read(buffer: ByteBuffer): Try[Array[Byte]] = {
      IOInt16.read(buffer).flatMap { size =>
        Try {
          val raw = Array.ofDim[Byte](size)
          buffer.get(raw)

          raw
        }
      }
    }

    override def write(buffer: ByteBuffer, data: Array[Byte]): Try[Unit] =
      Try {
        IOInt16.write(buffer, data.length.toShort)
        buffer.put(data)
      }

    override def sizeOf(data: Array[Byte]): Int =
      IOInt16.size + data.length
  }

  case object IOString extends VariableSizeIOType[String] {
    override def read(buffer: ByteBuffer): Try[String] =
      IOByteArray.read(buffer).map(raw => new String(raw))

    override def write(buffer: ByteBuffer, data: String): Try[Unit] =
      IOByteArray.write(buffer, data.getBytes(StandardCharsets.UTF_8))

    override def sizeOf(data: String): Int =
      IOByteArray.sizeOf(data.getBytes(StandardCharsets.UTF_8))
  }

  class IOArray[A: ClassTag](ioType: IOType[A]) extends VariableSizeIOType[Array[A]] {
    override def read(buffer: ByteBuffer): Try[Array[A]] =
      IOInt16.read(buffer).flatMap { length =>
        (0 until length)
          .map(i => i -> ioType.read(buffer))
          .foldLeft(Try(Array.ofDim[A](length))) {
            case (_, (_, Failure(e))) => Failure(e)
            case (Failure(e), _)      => Failure(e)
            case (Success(array), (i, Success(value))) =>
              array(i) = value
              Success(array)
          }
      }

    override def write(buffer: ByteBuffer, data: Array[A]): Try[Unit] = {
      IOInt16.write(buffer, data.length.toShort)
      checkSuccessful(data.map(d => ioType.write(buffer, d)))
    }

    override def sizeOf(data: Array[A]): Int =
      IOInt16.size + data.map(ioType.sizeOf).sum
  }

  class Structure private (
      val schema: IOSchema,
      val values: mutable.LinkedHashMap[String, Any]
  ) {
    def set(fieldName: String, value: Any): Option[Unit] =
      if (schema.contains(fieldName)) Some(values.update(fieldName, value))
      else None

    def get(fieldName: String): Try[Any] =
      values.get(fieldName)
        .toRight(new IllegalArgumentException(s"unknown field $fieldName"))
        .toTry

    override def toString: String =
      values.map { case (k, v) => s"$k=$v" }.mkString("Structure(", ", ", ")")
  }
  object Structure {
    def apply(schema: IOSchema, values: Seq[Any]): Structure =
      new Structure(
        schema,
        mutable.LinkedHashMap.from(
          schema.fields
            .zip(values)
            .map { case (f,v) => f.name -> v.asInstanceOf[f.ioType.Data] }
        )
      )
  }

  case class IOSchema(fields: IOSchema.Field[_]*)
      extends VariableSizeIOType[Structure] {
    val fieldNames: Set[String] = fields.map(_.name).toSet

    def newInstance(values: Any*): Structure = Structure(this, values.toSeq)
    def contains(fieldName: String): Boolean = fieldNames.contains(fieldName)

    override def read(buffer: ByteBuffer): Try[Structure] = {
      val values: Seq[(String, Try[_])] =
        fields.map { f =>
          f.name -> f.ioType.read(buffer)
        }

      values.foldLeft(Try(Structure(this, Seq.empty))) {
        case (Failure(e), _)      => Failure(e)
        case (_, (_, Failure(e))) => Failure(e)
        case (Success(s), (name, Success(value))) =>
          s.set(name, value)
            .map(_ => s)
            .toRight(new IllegalArgumentException(s"unknown parameter $name"))
            .toTry
      }
    }

    override def write(buffer: ByteBuffer, data: Structure): Try[Unit] =
      checkSuccessful(
        fields
          .zip(data.values.values)
          .map {
            case (f, v) =>
              f.ioType.write(buffer, v.asInstanceOf[f.ioType.Data])
          }
      )

    override def sizeOf(data: Structure): Int =
      fields
        .zip(data.values.values)
        .map { case (f, v) => f.ioType.sizeOf(v.asInstanceOf[f.ioType.Data]) }
        .sum
  }
  object IOSchema {
    case class Field[A](name: String, ioType: IOType[A])

    def Int8(name: String): Field[Byte]             = Field(name, IOInt8)
    def Int16(name: String): Field[Short]           = Field(name, IOInt16)
    def Int32(name: String): Field[Int]             = Field(name, IOInt32)
    def Int64(name: String): Field[Long]            = Field(name, IOInt64)
    def String(name: String): Field[String]         = Field(name, IOString)
    def ByteArray(name: String): Field[Array[Byte]] = Field(name, IOByteArray)
    def Structure(name: String, schema: IOSchema): Field[Structure] =
      Field(name, schema)
  }

  def checkSuccessful(l: Seq[Try[Unit]]): Try[Unit] =
    l.fold(Success(())) {
      case (Failure(e), _) => Failure(e)
      case (_, Failure(e)) => Failure(e)
      case _               => Success(())
    }
}
