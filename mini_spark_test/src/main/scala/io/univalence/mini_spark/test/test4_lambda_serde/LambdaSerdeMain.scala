package io.univalence.mini_spark.test.test4_lambda_serde

import java.io.{ByteArrayInputStream, ByteArrayOutputStream, ObjectInputStream, ObjectOutputStream}

object LambdaSerdeMain {
  def main(args: Array[String]): Unit = {
    val lambda1 = (x: Int) => x + 1
    val lambdaData1: Array[Byte] = serializeLambda(lambda1)

    val lambda2 = (x: Int) => x * 2
    val lambdaData2: Array[Byte] = serializeLambda(lambda2)

    runLambda(lambdaData1, 10)
    runLambda(lambdaData2, 10)
  }

  def runLambda(lambdaData: Array[Byte], n: Int): Unit = {
    val result: Int => Int = deserializeLambda(lambdaData)
    println(result(n))
  }

  def serializeLambda(lambda: Int => Int): Array[Byte] = {
    val baOut = new ByteArrayOutputStream()
    val out = new ObjectOutputStream(baOut)
    out.writeObject(lambda)

    val data = baOut.toByteArray
    baOut.close()
    data
  }

  def deserializeLambda(data: Array[Byte]): Int => Int = {
    val baIn = new ByteArrayInputStream(data)
    val in = new ObjectInputStream(baIn)

    in.readObject().asInstanceOf[Int => Int]
  }
}
