package io.univalence.mini_spark.test.test6_protocol

import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

class ActorSystem(executionContext: ExecutionContext) {
  val actors: ArrayBuffer[BaseActor]                = ArrayBuffer[BaseActor]()
  val actorsThreads: ArrayBuffer[Future[BaseActor]] = ArrayBuffer[Future[BaseActor]]()

  def add(actor: BaseActor): ActorRef = {
    actors += actor
    actorsThreads += Future { actor.start(); actor }(executionContext)

    (message: Message) => actor.inQueue.put(message)
  }

  def awaitTermination(): Future[Seq[BaseActor]] = {
    implicit val ec: ExecutionContext = executionContext

    val futures = Future.sequence(actorsThreads.toSeq)

    Await.ready(futures, Duration.Inf)
  }

  def stopAll(): Unit = {
    implicit val ec: ExecutionContext = executionContext

    actors.foreach(_.stop())
  }

}

trait ActorRef {
  def send(message: Message): Unit
}