package io.univalence.mini_spark.test.test5_grpc

import io.grpc.{Server, ServerBuilder}
import io.univalence.mini_spark.protocol.echo._
import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.ExecutionContext.Implicits._

object ServerMain {
  def main(args: Array[String]): Unit = {
    start()
    blockUntilShutdown()
  }

  var server: Server = _

  def start(): Unit = {
    val builder =
      ServerBuilder
        .forPort(19092)

    builder
      .addService(
        MasterGrpc.bindService(new ServerImpl, ExecutionContext.global)
      )

    server = builder.build.start

    sys.addShutdownHook {
      server.shutdown()
    }
  }

  def blockUntilShutdown(): Unit =
    if (server != null) {
      server.awaitTermination()
    }

  class ServerImpl extends MasterGrpc.Master {
    override def processEcho(request: EchoRequest): Future[EchoReply] =
      Future(EchoReply(request.message))
  }
}
