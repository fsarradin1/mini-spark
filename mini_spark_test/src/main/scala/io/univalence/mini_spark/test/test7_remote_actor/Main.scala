package io.univalence.mini_spark.test.test7_remote_actor

import io.univalence.mini_spark.test.test9_remote_actor_2.{ActorRef, ActorSystem, BaseRemoteActor, RemoteActorRef}
import scala.concurrent.ExecutionContext

object PingMain {
  def main(args: Array[String]): Unit = {
    val system = new ActorSystem(19090, "ping-actorsystem", ExecutionContext.global)
    sys.addShutdownHook(system.stop())

    val ping = system.register(new PingPongActor("localhost", 19090, "PING"))

    system.awaitTermination()
  }
}

object PongMain {
  def main(args: Array[String]): Unit = {
    val system = new ActorSystem(19091, "pong-actorsystem", ExecutionContext.global)
    sys.addShutdownHook(system.stop())

    val pong = system.register(new PingPongActor("localhost", 19091, "PONG"))
    val ping = RemoteActorRef("localhost", 19090, "PING")

    pong.send(ping, Ball)

    system.awaitTermination()
  }
}

class PingPongActor(host: String, port: Int, name: String)
    extends BaseRemoteActor(host, port, name) {
  override def receive(sender: ActorRef): PartialFunction[Any, Unit] = {
    case Ball =>
      println(name)
      Thread.sleep(500)
      sender ! Ball
  }
}

case object Ball
