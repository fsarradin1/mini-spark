package io.univalence.mini_spark.test.test3_serde_net

import EndPoint._
import io.univalence.mini_spark.test.test3_serde_net.Message._
import java.net.InetSocketAddress
import java.nio.channels.{SelectionKey, Selector, ServerSocketChannel, SocketChannel}
import scala.util.Using

object ServerMain {
  import EndPoint.serde._

  def main(args: Array[String]): Unit = {
    Using(ServerSocketChannel.open()) { serverSocket =>
      val selector = Selector.open()

      serverSocket.bind(new InetSocketAddress(19092))
      serverSocket.configureBlocking(false)
      serverSocket.register(selector, serverSocket.validOps())

      while (true) {
        serveLoop(serverSocket, selector)
      }
    }.get
  }

  private def serveLoop(
      serverSocket: ServerSocketChannel,
      selector: Selector
  ): Unit =
    if (selector.select(500) > 0) {
      val selectionKeys = selector.selectedKeys()
      val keys          = selectionKeys.iterator()

      while (keys.hasNext) {
        val key = keys.next()
        keys.remove()

        handleKey(serverSocket, selector, key)
      }
    }

  private def handleKey(
      server: ServerSocketChannel,
      selector: Selector,
      key: SelectionKey
  ): Unit = {
    if (key.isValid) {
      if (key.isAcceptable) {
        val client = server.accept()
        println(s"new client: ${client.getRemoteAddress}")
        client.configureBlocking(false)
        client.socket().setTcpNoDelay(true)
        client.socket().setKeepAlive(true)
        client.register(selector, client.validOps())
      }
      if (key.isReadable) {
        val client = key.channel().asInstanceOf[SocketChannel]

        val result = handleClient(client)
        result match {
          case Left(EndOfStream) =>
            println(s"connection with ${client.getRemoteAddress} closed...")
            client.close()
            key.cancel()

          case Left(UnknownException(_, e)) => throw e
          case Left(error) => println(error)
          case Right(_) => println(s"message sent to ${client.getRemoteAddress}")
        }
      }
    }
  }

  def handleClient(client: SocketChannel): Either[Error, Unit] =
    for {
      request  <- Message.readFrom1[Request](client)
      _        <- putStrLn(s"<--[] ${request.mkString}")
      response <- handleRequest(request)
      message  <- Message.from1(response, request.header.correlationId)
      _        <- putStrLn(s"-->[] ${message.mkString}")
      _        <- message.writeTo1(client)
    } yield ()

  def handleRequest(request: Message[Request]): Either[Error, Response] =
    request.payload match {
      case Echo.EchoRequest(message) =>
        Right(Echo.EchoResponse(message))
      case _ =>
        Left(
          UnknownException(
            request.header,
            new IllegalArgumentException(s"unkonwn request message: $request")
          )
        )
    }

//  def putStrLn(message: => String): Try[Unit] = Try { println(message) }

}
