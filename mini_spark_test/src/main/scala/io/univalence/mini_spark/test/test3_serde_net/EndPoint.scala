package io.univalence.mini_spark.test.test3_serde_net

object EndPoint {
  val endpoints: Map[Short, Class[_ <: Payload]] =
    Map(
      0x0000 -> classOf[Echo.EchoRequest],
      0x0100 -> classOf[Echo.EchoResponse],
      0x0001 -> classOf[Fetch.FetchRequest],
      0x0101 -> classOf[Fetch.FetchResponse]
    ).map { case (k, v) => k.toShort -> v }

  val endPointIds: Map[Short, String] =
    endpoints.map { case (k, v) => k.toShort -> v.getSimpleName }

  val reverseIds: Map[String, Short] = endPointIds.map(_.swap)

  val serde = new SerdeDerivation(endPointIds)
  import serde._

  type Raw = Array[Byte]

  sealed trait Payload
  sealed trait Request  extends Payload
  sealed trait Response extends Payload

  object Echo {
    case class EchoRequest(message: String)  extends Request
    case class EchoResponse(message: String) extends Response
  }
  object Fetch {
    case class FetchRequest(topic: String, partitionId: Short, offset: Long)
        extends Request
    case class FetchResponse(records: Array[Record]) extends Response
  }

  case class Record(
      topic: String,
      partitionId: Short,
      offset: Long,
      key: Raw,
      value: Raw
  )

  case class Header(
      magic: Short,
      correlationId: Int,
      endpointId: Short,
      payloadSize: Int
  ) {
    def mkString: String =
      f"header - magic:0x$magic%04x correlationId:0x$correlationId%08x endpointId:0x$endpointId%04x payloadSize: $payloadSize%d"
  }
  object Header {
    val Null: Header  = Header(0, 0, 0, 0)
    val byteSize: Int = Header.Null.byteSize
  }

}
