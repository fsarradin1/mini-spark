package io.univalence.mini_spark.test.test3_serde_net

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import magnolia.{CaseClass, Magnolia, SealedTrait}
import scala.language.experimental.macros
import scala.reflect.ClassTag
import scala.util.{Failure, Success, Try}

trait Serde[A] {
  def write(buffer: ByteBuffer, a: A): Try[Unit]

  def readFrom(buffer: ByteBuffer): Try[A]

  def sizeOf(a: A): Int
}
trait BasicSerde {

  @inline
  def apply[A: Serde]: Serde[A] = implicitly

  implicit class SerdeWriteOps[A: Serde](a: A) {
    def writeTo(buffer: ByteBuffer): Try[Unit] =
      implicitly[Serde[A]].write(buffer, a)

    def byteSize: Int = implicitly[Serde[A]].sizeOf(a)
  }

  implicit val byteSerde: Serde[Byte] =
    new Serde[Byte] {
      override def write(buffer: ByteBuffer, a: Byte): Try[Unit] =
        Try {
          buffer.put(a)
        }

      override def readFrom(buffer: ByteBuffer): Try[Byte] =
        Try {
          buffer.get()
        }

      val size: Int = 1

      override def sizeOf(a: Byte): Int = size
    }

  implicit val shortSerde: Serde[Short] =
    new Serde[Short] {
      override def write(buffer: ByteBuffer, a: Short): Try[Unit] =
        Try {
          buffer.putShort(a)
        }

      override def readFrom(buffer: ByteBuffer): Try[Short] =
        Try {
          buffer.getShort()
        }

      val size: Int = 2

      override def sizeOf(a: Short): Int = size
    }

  implicit val intSerde: Serde[Int] =
    new Serde[Int] {
      override def write(buffer: ByteBuffer, a: Int): Try[Unit] =
        Try {
          buffer.putInt(a)
        }

      override def readFrom(buffer: ByteBuffer): Try[Int] =
        Try {
          buffer.getInt()
        }

      val size: Int = 4

      override def sizeOf(a: Int): Int = size
    }

  implicit val longSerde: Serde[Long] =
    new Serde[Long] {
      override def write(buffer: ByteBuffer, a: Long): Try[Unit] =
        Try {
          buffer.putLong(a)
        }

      override def readFrom(buffer: ByteBuffer): Try[Long] =
        Try {
          buffer.getLong()
        }

      val size: Int = 8

      override def sizeOf(a: Long): Int = size
    }

  implicit val stringSerde: Serde[String] =
    new Serde[String] {
      override def write(buffer: ByteBuffer, a: String): Try[Unit] =
        a.getBytes(StandardCharsets.UTF_8).writeTo(buffer)

      override def readFrom(buffer: ByteBuffer): Try[String] =
        implicitly[Serde[Array[Byte]]]
          .readFrom(buffer)
          .map(ba => new String(ba))

      override def sizeOf(a: String): Int =
        implicitly[Serde[Array[Byte]]]
          .sizeOf(a.getBytes(StandardCharsets.UTF_8))
    }

  implicit val byteArraySerde: Serde[Array[Byte]] =
    new Serde[Array[Byte]] {
      override def write(buffer: ByteBuffer, a: Array[Byte]): Try[Unit] =
        Try {
          a.length.toShort.writeTo(buffer)
          buffer.put(a)
        }

      override def readFrom(buffer: ByteBuffer): Try[Array[Byte]] = {
        implicitly[Serde[Short]].readFrom(buffer).flatMap { size =>
          Try {
            val raw = Array.ofDim[Byte](size)
            buffer.get(raw)

            raw
          }
        }
      }

      override def sizeOf(a: Array[Byte]): Int =
        implicitly[Serde[Short]].sizeOf(0) + a.length
    }

  implicit def array[A: Serde: ClassTag]: Serde[Array[A]] =
    new Serde[Array[A]] {
      override def write(buffer: ByteBuffer, a: Array[A]): Try[Unit] = {
        a.length.toShort.writeTo(buffer)
        checkSuccessful(a.map(d => d.writeTo(buffer)))
      }

      override def readFrom(buffer: ByteBuffer): Try[Array[A]] =
        implicitly[Serde[Short]].readFrom(buffer).flatMap { length =>
          (0 until length)
            .map(i => i -> implicitly[Serde[A]].readFrom(buffer))
            .foldLeft(Try(Array.ofDim[A](length))) {
              case (_, (_, Failure(e))) => Failure(e)
              case (Failure(e), _)      => Failure(e)
              case (Success(array), (i, Success(value))) =>
                array(i) = value
                Success(array)
            }
        }

      override def sizeOf(a: Array[A]): Int =
        implicitly[Serde[Short]]
          .sizeOf(0) + a.map(implicitly[Serde[A]].sizeOf).sum
    }

  def checkSuccessful(l: Seq[Try[Unit]]): Try[Unit] =
    l.fold(Success(())) {
      case (Failure(e), _) => Failure(e)
      case (_, Failure(e)) => Failure(e)
      case _               => Success(())
    }
}

object Serde extends BasicSerde

class SerdeDerivation(mapper: Map[Short, String]) extends BasicSerde {

  val reverseMapper: Map[String, Short] =
    mapper.map(_.swap)

  type Typeclass[A] = Serde[A]

  def combine[A](ctx: CaseClass[Typeclass, A]): Typeclass[A] =
    new Typeclass[A] {
      override def write(buffer: ByteBuffer, a: A): Try[Unit] = {
        val typeID =
          reverseMapper
            .get(a.getClass.getSimpleName)

        for {
          _ <- Try {
            typeID.map(t => shortSerde.write(buffer, t)).getOrElse(())
          }
          _ <- checkSuccessful(
            ctx.parameters.map { p =>
              p.typeclass.write(buffer, p.dereference(a))
            }
          )
        } yield ()
      }

      override def readFrom(buffer: ByteBuffer): Try[A] = {
        // FIXME: this trick seems to help in getting non-truncated comm between client and server
        Thread.sleep(1)

        val typeId = ctx.typeName.short
        val hasId  = reverseMapper.contains(typeId)

        for {
          typeId <- Try {
            if (hasId) Option(shortSerde.readFrom(buffer).get) else None
          }
          if typeId.forall(mapper.contains)
          a <-
            ctx
              .constructEither { p =>
                p.typeclass.readFrom(buffer).toEither
              }
              .left
              .map(_.head)
              .toTry
        } yield a
      }

      override def sizeOf(a: A): Int =
        (reverseMapper
          .get(a.getClass.getSimpleName)
          .map(_ => shortSerde.sizeOf(0))
          .getOrElse(0)
          + ctx.parameters.map { p =>
            p.typeclass.sizeOf(p.dereference(a))
          }.sum)
    }

  def dispatch[A](ctx: SealedTrait[Typeclass, A]): Typeclass[A] =
    new Typeclass[A] {
      override def write(buffer: ByteBuffer, a: A): Try[Unit] =
        ctx.dispatch(a) { subtype =>
          subtype.typeclass.write(buffer, subtype.cast(a))
        }

      override def readFrom(buffer: ByteBuffer): Try[A] =
        for {
          id <- shortSerde.readFrom(buffer)
          _  <- Try { buffer.position(buffer.position() - shortSerde.sizeOf(0)) }
          name <-
            mapper
              .get(id)
              .toRight(new TypeNotPresentException(f"id=0x$id%04x", null))
              .toTry
          subtype <-
            ctx.subtypes
              .find(_.typeName.short == name)
              .toRight(new TypeNotPresentException(name, null))
              .toTry
          value <- subtype.typeclass.readFrom(buffer)
        } yield value

      override def sizeOf(a: A): Int =
        ctx.dispatch(a) { subtype =>
          subtype.typeclass.sizeOf(subtype.cast(a))
        }
    }

  implicit def gen[T]: Serde[T] = macro Magnolia.gen[T]
}
