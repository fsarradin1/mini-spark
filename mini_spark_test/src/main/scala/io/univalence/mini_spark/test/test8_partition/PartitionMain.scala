package io.univalence.mini_spark.test.test8_partition

import io.univalence.mini_spark.rdd.{FilePartition, SplitFileReaderIterator}
import java.io.{File, FileInputStream}
import java.nio.file.Files
import scala.io.Source
import scala.util.Using

object PartitionMain {

  def main(args: Array[String]): Unit = {
    val filepath = "data/lieux-de-tournage-a-paris.csv"
//    val filepath = "data/small.csv"
    val file = new File(filepath)
    file.length()

    val fileSize = Files.size(file.toPath)
    println(s"file size: $fileSize")
    val partitions = FilePartition.getPartitions(file.toString, 5)

    println(partitions.map(_.length).sum)

    val result =
      partitions.map { partition =>
        Using(new FileInputStream(partition.path)) { file =>
          val iterator = new SplitFileReaderIterator(
            file,
            partition.startOffset,
            partition.length
          ).toList

          println(s"$partition")
//          iterator.foreach(println)

          partition -> iterator.length
        }.get
      }

    result.foreach {
      case (partition, lineCount) =>
        println(
          s"partition (${partition.startOffset}, ${partition.length}): ${lineCount}"
        )
    }

    println(s"total: ${result.map(_._2).sum}")

    val lineCount =
      Using(Source.fromFile(file)) { content =>
        content.getLines().length
      }

    println(s"true line count: $lineCount")
  }

}
