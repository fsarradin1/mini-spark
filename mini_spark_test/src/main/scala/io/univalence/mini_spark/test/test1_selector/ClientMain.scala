package io.univalence.mini_spark.test.test1_selector

import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.SocketChannel
import java.nio.charset.StandardCharsets
import scala.util.Using

object ClientMain {

  def main(args: Array[String]): Unit = {
    Using(SocketChannel.open()) { server =>
      println("connecting to server...")
      server.connect(new InetSocketAddress("127.0.0.1", 19092))

      println("sending request")
      val request       = "Hello".getBytes(StandardCharsets.UTF_8)
      val requestBuffer = ByteBuffer.allocate(4 + request.length)
      requestBuffer.putInt(request.length)
      requestBuffer.put(request)
      requestBuffer.rewind()
      server.write(requestBuffer)

      println("getting response")
      val lengthBuffer = ByteBuffer.allocate(4)
      server.read(lengthBuffer)
      lengthBuffer.rewind()
      val length = lengthBuffer.getInt()
      val buffer = ByteBuffer.allocate(length)
      server.read(buffer)
      buffer.rewind()
      val data = new String(buffer.array())
      println(data)

    //      requestBuffer.rewind()
    //      server.write(requestBuffer)
    }.get
  }

}
