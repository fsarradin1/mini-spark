package io.univalence.mini_spark.test.test6_protocol

import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.{ArrayBlockingQueue, BlockingQueue}
import java.util.{Timer, TimerTask}
import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

trait Actor {
  val ref: ActorRef

  // represents the behavior of the actor
  def receive(sender: ActorRef): PartialFunction[Any, Unit]

  // add a task to execute periodically
  def schedule(task: () => Unit, period: Long): Unit

  // code to execute when the actor starts
  def onStart(): Unit = {}

  // code to execute when the actor stops
  def onStop(): Unit = {}

  // used by actor system to launch/shutdown actor
  def start(): Unit
  def stop(): Unit
}

abstract class BaseActor extends Actor {
  val inQueue: BlockingQueue[Message] = new ArrayBlockingQueue[Message](1024)
  val stopped: AtomicBoolean          = new AtomicBoolean(true)
  val timerTasks: ArrayBuffer[(TimerTask, Long)] =
    ArrayBuffer[(TimerTask, Long)]()
  val timer: Timer = new Timer()

  override val ref: ActorRef = (message: Message) => inQueue.put(message)

  override def onStart(): Unit = {}
  override def onStop(): Unit  = {}

  def send(message: Message): Unit =
    inQueue.put(message)

  override def start(): Unit = {
    @tailrec
    def loop(): Unit = {
      if (stopped.get()) () // block if no message
      else {
        val message = inQueue.take()
//        println(s"${getClass.getSimpleName}: got message: $message")
        receive(message.sender)(message.payload)

        loop()
      }
    }

    startScheduler() // for scheduled tasks
    stopped.set(false)
    onStart() // executes code to run on start
    loop()    // actor infinte loop
  }

  private def startScheduler(): Unit =
    timerTasks.foreach {
      case (task, period) =>
        timer.schedule(task, 1000 + Random.nextInt(500), period)
    }

  def schedule(task: () => Unit, period: Long): Unit = {
    val timerTask =
      new TimerTask {
        override def run(): Unit = task()
      }

    timerTasks += (timerTask -> period)
    if (!stopped.get())
      timer.schedule(timerTask, 1000, period)
  }

  override def stop(): Unit = {
    if (stopped.compareAndSet(false, true)) {
      timerTasks.foreach(_._1.cancel())
      timer.purge()
      timer.cancel()
      onStop()
    }
  }

}

object ExampleActorMain {

  def main(args: Array[String]): Unit = {
    val system = new ActorSystem(scala.concurrent.ExecutionContext.global)
    sys.addShutdownHook(system.stopAll()) // graceful shutdown
    // create actor and add it to actor system
    val helloActor = system.add(new HelloActor)

    // send message to actor (the sender is unknown)
    helloActor.send(Message(null, "John"))

    system.awaitTermination()
  }

  class HelloActor extends BaseActor {
    override def receive(sender: ActorRef): PartialFunction[Any, Unit] = {
      case payload => println(s"Hello, $payload!")
    }
  }

}

object PingPongMain {

  def main(args: Array[String]): Unit = {
    val system = new ActorSystem(scala.concurrent.ExecutionContext.global)
    sys.addShutdownHook(system.stopAll())

    val ping = system.add(new PingPongActor("PING"))
    val pong = system.add(new PingPongActor("PONG"))

    ping.send(Message(pong, Ball))

    system.awaitTermination()
  }

  case object Ball

  class PingPongActor(text: String) extends BaseActor {
    override def receive(sender: ActorRef): PartialFunction[Any, Unit] = {
        case Ball =>
          println(text)
          Thread.sleep(500)
          sender.send(Message(ref, Ball))
      }
  }

}
