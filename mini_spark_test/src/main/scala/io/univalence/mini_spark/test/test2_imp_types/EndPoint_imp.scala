package io.univalence.mini_spark.test.test2_imp_types

import io.univalence.mini_spark.test.test2_imp_types.IOType._
import java.nio.ByteBuffer
import scala.util.Try

sealed trait EndPoint_imp {
  val name: String
  val id: Int

}
object EndPoint_imp {
  val endpoints: Map[Class[_ <: EndPoint_imp], EndPointInfo] =
    Map(
      Echo.getClass  -> EndPointInfo(0x00, "ECHO"),
      Fetch.getClass -> EndPointInfo(0x01, "FETCH")
    )

//  trait MessagePartWriter[A] {
//    val schema: IOSchema
//
//    def toStructure(a: A): Structure
//
//    def write(buffer: ByteBuffer, a: A): Try[Unit] =
//      schema.write(buffer, toStructure(a))
//  }

//  trait MessagePartReader[A] {
//    val schema: IOSchema
//
//    def fromStructure(structure: Structure): Try[A]
//
//    def read(buffer: ByteBuffer): Try[A] =
//      schema
//        .read(buffer)
//        .flatMap(fromStructure)
//  }

  case object Echo extends EndPoint_imp { outer =>
    override val name: String = endpoints(getClass).name
    override val id: Int      = endpoints(getClass).id

    case class EchoRequest(message: String) {
      def toStructure: Structure =
        Structure(EchoRequest.schema, Seq[Any](message))

      def write(buffer: ByteBuffer): Try[Unit] =
        EchoRequest.schema.write(buffer, toStructure)
    }
    object EchoRequest {
      val schema: IOSchema = IOSchema(
        IOSchema.String("message")
      )

      def fromStructure(structure: Structure): Try[EchoRequest] =
        for {
          message <- structure.get("message").map(_.asInstanceOf[String])
        } yield EchoRequest(message)

      def read(buffer: ByteBuffer): Try[EchoRequest] =
        schema
          .read(buffer)
          .flatMap(fromStructure)
    }

    case class EchoResponse(message: String)
    object EchoResponse {
      val schema: IOSchema = IOSchema(
        IOSchema.String("message")
      )
    }
  }

  case object Fetch extends EndPoint_imp {
    override val name: String = endpoints(getClass).name
    override val id: Int      = endpoints(getClass).id
  }

  case class EndPointInfo(id: Int, name: String)
}

case class Header(
    magic: Short,
    endpointId: Byte,
    correlationId: Int,
    messageSize: Int
) {
  def toStructure: Structure =
    Structure(
      Header.Schema,
      Seq[Any](magic, endpointId, correlationId, messageSize)
    )
}
object Header {
  val magic: Short = 0x8e2a.toShort

  val Schema: IOSchema = IOSchema(
    IOSchema.Int16("magic"),
    IOSchema.Int8("endpoint_id"),
    IOSchema.Int32("correlation_id"),
    IOSchema.Int32("message_size")
  )

  val nullHeaderStruct: Structure =
    Structure(
      Schema,
      Seq[Any](0x0000.toShort, 0x00.toByte, 0x00000000.toInt, 0x00000000.toInt)
    )
  val headerSize: Int = Schema.sizeOf(nullHeaderStruct)

  def fromStructure(structure: Structure): Try[Header] =
    for {
      magic <- structure.get("magic").map(_.asInstanceOf[IOInt16.Data])
      endpointId <-
        structure.get("endpoint_id").map(_.asInstanceOf[IOInt8.Data])
      correlationId <-
        structure.get("correlation_id").map(_.asInstanceOf[IOInt32.Data])
      messageSize <-
        structure.get("message_size").map(_.asInstanceOf[IOInt32.Data])
    } yield Header(magic, endpointId, correlationId, messageSize)

  def read(buffer: ByteBuffer): Try[Header] =
    Schema
      .read(buffer)
      .flatMap(fromStructure)

  def write(buffer: ByteBuffer, header: Header): Try[Unit] =
    Schema.write(buffer, header.toStructure)
}
