package io.univalence.mini_spark.test.test9_remote_actor_2

import com.google.protobuf.ByteString
import io.grpc.{ManagedChannel, ManagedChannelBuilder}
import io.univalence.mini_spark.actor.actor.{ActorEndPointGrpc, NetActorMessage, NetActorRef}
import java.io.{ByteArrayOutputStream, ObjectOutputStream}

trait ActorRef {
  val name: String
  def send(sender: ActorRef, payload: Any): Unit
  def !(payload: Any)(implicit sender: ActorRef): Unit = send(sender, payload)
}

case class LocalActorRef(
    actor: Actor,
    override val name: String
) extends ActorRef {
  override def send(sender: ActorRef, payload: Any): Unit = {
    val message = ActorMessage(sender, payload)
    actor.inbox.put(message)
  }
}

case class RemoteActorRef(
    host: String,
    port: Int,
    override val name: String
) extends ActorRef {

  override def send(sender: ActorRef, payload: Any): Unit = {
    if (!sender.isInstanceOf[RemoteActorRef]) {
      throw new IllegalArgumentException("sender should also be remote")
    }
    val remoteSender = sender.asInstanceOf[RemoteActorRef]

//    println(s"$this: emitting $payload from $sender to $this")

    val data = ByteString.copyFrom(serialize(payload))

    val channel: ManagedChannel = connect(host, port)
    try {
      ActorEndPointGrpc
        .blockingStub(channel)
        .receive(
          NetActorMessage(
            sender = Option(NetActorRef(remoteSender.host, remoteSender.port, remoteSender.name)),
            receiver = Option(NetActorRef(host, port, name)),
            payload = data
          )
        )
    } finally {
      channel.shutdown()
    }
  }

  def stop(): Unit = ()

  private def serialize(payload: Any): Array[Byte] = {
    val bos = new ByteArrayOutputStream()
    try {
      val oos = new ObjectOutputStream(bos)
      oos.writeObject(payload)

      bos.toByteArray
    } finally {
      bos.close()
    }
  }

  private def connect(host: String, port: Int) = {
    val builder = ManagedChannelBuilder.forAddress(host, port)
    builder.usePlaintext()
    builder.build()
  }
}
