package io.univalence.mini_spark.test.test3_serde_net

import EndPoint.{Echo, Response}
import java.net.InetSocketAddress
import java.nio.channels.SocketChannel
import java.security.SecureRandom
import scala.util.{Failure, Success, Try, Using}

object ClientMain {
  import EndPoint.serde._

  val random = new SecureRandom()

  def main(args: Array[String]): Unit = {
    Using(SocketChannel.open(new InetSocketAddress("127.0.0.1", 19092))) {
      socket =>
        val correlationId = random.nextInt()

        for {
          request  <- Message.from(Echo.EchoRequest("hello"), correlationId)
          _        <- putStrLn(s"-->[] ${request.mkString}")
          _        <- request.writeTo(socket)
          response <- Message.readFrom[Response](socket)
          _        <- checkCorrelationId(correlationId, response.header.correlationId)
          _        <- putStrLn(s"<--[] ${response.mkString}")
        } yield response
    }.get
  }

  def checkCorrelationId(currentCI: Int, receivedCI: Int): Try[Unit] =
    if (currentCI == receivedCI)
      Success(())
    else
      Failure(
        new IllegalArgumentException(
          s"invalid correlation ID. Should be $currentCI, got $receivedCI"
        )
      )

  def putStrLn(message: => String): Try[Unit] = Try { println(message) }
}
