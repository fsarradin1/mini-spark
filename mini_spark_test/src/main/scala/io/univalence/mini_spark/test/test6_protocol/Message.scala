package io.univalence.mini_spark.test.test6_protocol

case class Message(sender: ActorRef, payload: Any)
