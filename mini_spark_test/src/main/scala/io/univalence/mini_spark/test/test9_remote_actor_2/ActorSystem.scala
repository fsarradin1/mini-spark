package io.univalence.mini_spark.test.test9_remote_actor_2

import com.google.protobuf.empty.Empty
import io.grpc.{Server, ServerBuilder}
import io.univalence.mini_spark.actor.actor.ActorEndPointGrpc.ActorEndPoint
import io.univalence.mini_spark.actor.actor.{Ack, ActorEndPointGrpc, ActorNames, NetActorMessage}
import java.io.{ByteArrayInputStream, ObjectInputStream}
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * Manager of actors for this host and port.
  *
  * @param port port to listen for message coming from remote actors.
  * @param name name os this actor system
  * @param executionContext thread pool to use
  */
class ActorSystem(port: Int, name: String, executionContext: ExecutionContext) {
  private implicit val ec: ExecutionContext             = executionContext
  private val actors: mutable.Map[ActorRef, Actor]      = mutable.Map()
  private val actorNames: mutable.Map[String, ActorRef] = mutable.Map()
  private val threads: ArrayBuffer[Future[Actor]]       = ArrayBuffer()
  private val server: Server                            = createServer(port)

  locally {
    println(s"$name: start server on $port")
    server.start()
  }

  /**
    * Register an actor, starts it, and get a reference on it.
    *
    *
    */
  def register(actor: Actor): ActorRef = {
    val ref = createRefFrom(actor)
    actors += (ref            -> actor)
    actorNames += (actor.name -> ref)
    val thread = Future { actor.start(); actor }
    threads += thread

    println(s"$name: registered $ref")

    ref
  }

  def awaitTermination(): Unit = {
    val allThreads = Future.sequence(threads)

    Await.ready(allThreads, Duration.Inf)
    server.awaitTermination()
  }

  def stop(): Unit = {
    server.shutdownNow()
    actors.values.foreach(_.stop())
    actors.keys.foreach {
      case remote: RemoteActorRef => remote.stop()
      case _                      => ()
    }

    println(s"$name: stopped")
  }

  private def createRefFrom(actor: Actor): ActorRef =
    actor match {
      case _: BaseLocalActor => LocalActorRef(actor, actor.name)
      case remote: BaseRemoteActor =>
        RemoteActorRef(remote.host, remote.port, remote.name)
    }

  def createServer(port: Int): Server = {
    val service =
      ActorEndPointGrpc.bindService(new RemoteHandler, ExecutionContext.global)
    val server = ServerBuilder.forPort(port)
    server.addService(service)

    server.build()
  }

  class RemoteHandler extends ActorEndPoint {
    override def receive(request: NetActorMessage): Future[Ack] = {
      val requestReceiver = request.receiver.get
      val requestSender   = request.sender.get
      val receiver =
        RemoteActorRef(
          requestReceiver.host,
          requestReceiver.port,
          requestReceiver.name
        )
      val sender =
        RemoteActorRef(
          requestSender.host,
          requestSender.port,
          requestSender.name
        )
      val data = Array.ofDim[Byte](request.payload.size())
      request.payload.copyTo(data, 0)
      val payload = deserialize(data)

//      println(s"$name: received $payload from $sender to $receiver")

      actors
        .get(receiver)
        .fold(
          throw new NoSuchElementException(
            s"actor $receiver does not exist among ${actors.keys}"
          )
        )(
          _.inbox.put(ActorMessage(sender, payload))
        )

      Future.successful(Ack())
    }

    def deserialize(payload: Array[Byte]): Any = {
      val bis = new ByteArrayInputStream(payload)
      try {
        val ois = new ObjectInputStream(bis)

        ois.readObject()
      } finally { bis.close() }
    }

    override def actorNames(request: Empty): Future[ActorNames] = ???
  }

}
