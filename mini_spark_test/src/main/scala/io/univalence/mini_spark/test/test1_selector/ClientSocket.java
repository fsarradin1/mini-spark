package io.univalence.mini_spark.test.test1_selector;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;

public class ClientSocket {

    public static void main(String[] args) throws Exception {
        try (SocketChannel server = SocketChannel.open()) {
            server.connect(new InetSocketAddress("127.0.0.1", 19092));

            ByteBuffer buffer = ByteBuffer.allocate(1024);
            buffer.put("hello".getBytes(StandardCharsets.UTF_8));
            buffer.rewind();
            server.write(buffer);

            buffer.clear();
            server.read(buffer);
            buffer.rewind();
            System.out.println("response: " + new String(buffer.array()));
        }
    }

}
