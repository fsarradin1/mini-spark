package io.univalence.mini_spark.test.test9_remote_actor_2

import java.util.concurrent.BlockingQueue

/**
  * Interface to implement by actors
  */
trait Actor {

  /**
    * Reception collection for incoming messages.
    *
    * We use a blocking queue in a view to make actor wait for new
    * messages, if none are available.
    */
  private[test9_remote_actor_2] val inbox: BlockingQueue[ActorMessage]

  /**
    * Reference on this actor.
    *
    * This helps an actor to send a message to itself.
    */
  implicit val self: ActorRef

  /** Actor name. */
  val name: String

  def receive(sender: ActorRef): PartialFunction[Any, Unit]

  /**
    * Add a task to execute periodically.
    *
    * @param task task to execute periodically.
    * @param period period to wait between the end of the previous
    *               execution till the next one.
    */
  def schedule(task: () => Unit, period: Long): Unit

  def start(): Unit

  /** Gracefully stop this actor. */
  def stop(): Unit

  /** Kill not only this actor but also all the JVM. */
  def kill(): Unit

  def onStart(): Unit
  def onStop(): Unit
}
