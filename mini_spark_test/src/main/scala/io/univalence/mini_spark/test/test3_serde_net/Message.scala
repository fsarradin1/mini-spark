package io.univalence.mini_spark.test.test3_serde_net

import io.univalence.mini_spark.test.test3_serde_net.EndPoint.Header
import io.univalence.mini_spark.test.test3_serde_net.Message._
import java.io.EOFException
import java.nio.ByteBuffer
import java.nio.channels.{ByteChannel, SocketChannel}
import scala.util.{Failure, Success, Try}

case class Message[A: Serde](header: Header, payload: A) {

  def writeTo(channel: ByteChannel): Try[Unit] =
    Try {
      import EndPoint.serde._

      if (header.magic != magic)
        throw new IllegalArgumentException(
          f"bad header magic 0x${header.magic}%04x"
        )
      if (!EndPoint.endPointIds.contains(header.endpointId))
        throw new TypeNotPresentException(f"id=${header.endpointId}%04x", null)

      val headerBuffer = ByteBuffer.allocate(header.byteSize)
      header.writeTo(headerBuffer)
      headerBuffer.flip()
      channel.write(headerBuffer)
      val buffer = ByteBuffer.allocate(payload.byteSize)
      payload.writeTo(buffer)
      buffer.flip()
      channel.write(buffer)
    }

  def writeTo1(channel: ByteChannel): Either[Error, Unit] = {
    import EndPoint.serde._

    for {
      _ <- isTrueOr(header.magic == magic, MagicNumberUnknown(header))
      _ <- isTrueOr(
        EndPoint.endPointIds.contains(header.endpointId),
        EndPointIDUnknown(header)
      )
      headerBuffer  <- run(header, ByteBuffer.allocate(header.byteSize))
      _             <- run(header, { header.writeTo(headerBuffer); headerBuffer.rewind() })
      headerSize    <- run(header, { channel.write(headerBuffer) })
      _             <- isTrueOr(headerSize >= 0, EndOfStream)
      payloadBuffer <- run(header, ByteBuffer.allocate(payload.byteSize))
      _             <- run(header, { payload.writeTo(payloadBuffer); payloadBuffer.rewind() })
      payloadSize   <- run(header, { channel.write(payloadBuffer) })
      _             <- isTrueOr(payloadSize >= 0, EndOfStream)
    } yield ()
  }

  def mkString: String =
    s"Message[${header.mkString} - $payload]"
}

object Message {
  import EndPoint.{Header, endPointIds, reverseIds}

  val magic: Short = 0xfc5a.toShort

  sealed trait Error
  case class MagicNumberUnknown(header: Header)           extends Error
  case class EndPointIDUnknown(header: Header)            extends Error
  case class EndPointUnknown(name: String)                extends Error
  case class InsufficientHeaderData(exception: Throwable) extends Error
  case class InsufficientData(header: Header, exception: Throwable)
      extends Error
  case class UnknownHeaderExcception(exception: Throwable) extends Error
  case class UnknownException(header: Header, exception: Throwable)
      extends Error
  case object EndOfStream extends Error

  def from[A: Serde](payload: A, correlationId: Int): Try[Message[A]] = {
    import EndPoint.serde._

    val endpointName = payload.getClass.getSimpleName
    val endpointId   = reverseIds.get(endpointName)
    val message =
      endpointId
        .map(epId =>
          Message(
            Header(
              magic,
              correlationId,
              epId,
              payload.byteSize
            ),
            payload
          )
        )

    definedOr(message, new TypeNotPresentException(endpointName, null))
  }

  def from1[A: Serde](
      payload: A,
      correlationId: Int
  ): Either[Error, Message[A]] = {
    import EndPoint.serde._

    val endpointName = payload.getClass.getSimpleName
    val message =
      for {
        endpointId <- reverseIds.get(endpointName)
      } yield {
        val header = Header(magic, correlationId, endpointId, payload.byteSize)

        Message(header, payload)
      }

    message.toRight(EndPointUnknown(endpointName))
  }

  def headerExceptionToError(exception: Throwable): Error =
    exception match {
      case _: java.nio.BufferUnderflowException =>
        InsufficientHeaderData(exception)
      case _: java.nio.BufferOverflowException =>
        InsufficientHeaderData(exception)
      case _ => UnknownHeaderExcception(exception)
    }

  def exceptionToError(header: Header, exception: Throwable): Error =
    exception match {
      case _: java.nio.BufferUnderflowException =>
        InsufficientData(header, exception)
      case _: java.nio.BufferOverflowException =>
        InsufficientData(header, exception)
      case _ => UnknownException(header, exception)
    }

  def readFrom1[A: Serde](channel: SocketChannel): Either[Error, Message[A]] = {
    import EndPoint.serde._

    val headerBuffer = ByteBuffer.allocate(Header.byteSize)
    if (channel.read(headerBuffer) < 0)
      Left(EndOfStream)
    else {
      headerBuffer.rewind()

      for {
        header <- headerTryToEither(Serde[Header].readFrom(headerBuffer))
        _      <- putStrLn(s"header: ${header.mkString}")
        _      <- isTrueOr(header.magic == magic, MagicNumberUnknown(header))
        _ <- isTrueOr(
          endPointIds.contains(header.endpointId),
          EndPointIDUnknown(header)
        )
        buffer <- run(header, ByteBuffer.allocate(header.payloadSize))
        payloadIsEOS <- run(
          header, {
            val n = channel.read(buffer); buffer.rewind(); n < 0
          }
        )
        _       <- isTrueOr(!payloadIsEOS, EndOfStream)
        payload <- tryToEither(header, Serde[A].readFrom(buffer))
      } yield Message(header, payload)
    }
  }

  def readFrom[A: Serde](channel: ByteChannel): Try[Message[A]] = {
    import EndPoint.serde._

    val headerBuffer = ByteBuffer.allocate(Header.byteSize)
    val isEOS        = channel.read(headerBuffer) < 0

    if (isEOS)
      Failure(new EOFException())
    else {
      headerBuffer.rewind()

      for {
        header <- Serde[Header].readFrom(headerBuffer)
        _      <- tryPutStrLn(s"header: ${header.mkString}")
        _ <- tryIsTrueOr(
          header.magic == magic,
          new IllegalArgumentException(
            f"bad header magic 0x${header.magic}%04x in ${header.mkString}"
          )
        )
        _ <- tryIsTrueOr(
          endPointIds.contains(header.endpointId),
          new TypeNotPresentException(
            f"id=0x${header.endpointId}%04x in ${header.mkString}",
            null
          )
        )
        buffer <- Try {
          ByteBuffer.allocate(header.payloadSize)
        }
        payloadIsEOS <- Try {
          val n = channel.read(buffer); buffer.rewind(); n < 0
        }
        _       <- tryIsTrueOr(!payloadIsEOS, new EOFException())
        payload <- Serde[A].readFrom(buffer)
      } yield Message(header, payload)
    }
  }

  def tryIsTrueOr(b: Boolean, exception: Exception): Try[Unit] =
    if (b) Success(())
    else Failure(exception)

  def isTrueOr(b: Boolean, error: Error): Either[Error, Unit] =
    if (b) Right(())
    else Left(error)

  def definedOr[A](option: Option[A], exception: Exception): Try[A] =
    option.toRight(exception).toTry

  def headerTryToEither[A](tryA: => Try[A]): Either[Error, A] =
    tryA.toEither.left.map(headerExceptionToError)

  def tryToEither[A](header: Header, tryA: => Try[A]): Either[Error, A] =
    tryA.toEither.left.map(e => exceptionToError(header, e))

  def run[A](header: Header, process: => A): Either[Error, A] =
    tryToEither(header, Try { process })

  def tryPutStrLn(message: => String): Try[Unit] = Try { println(message) }
  def putStrLn[L](message: => String): Either[L, Unit] =
    Right { println(message) }
}
