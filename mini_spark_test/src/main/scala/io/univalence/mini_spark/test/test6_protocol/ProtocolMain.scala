package io.univalence.mini_spark.test.test6_protocol

import java.time.{Instant, Duration => JDuration}
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext

object ProtocolMain {

  def main(args: Array[String]): Unit = {
    val executionContext = ExecutionContext.global
    val system           = new ActorSystem(executionContext)
    sys.addShutdownHook(system.stopAll())

    val driver = system.add(new Driver)

    val executor1 = system.add(new Executor("1", driver))
    val executor2 = system.add(new Executor("2", driver))
    val executor3 = system.add(new Executor("3", driver))

    Thread.sleep(1000)
    driver.send(Message(null, Parallelize(Seq.range(0, 200))))
    Thread.sleep(1000)
    driver.send(Message(null, MapTask(serializeLambda((x: Int) => x * x))))
    driver.send(Message(null, MapTask(serializeLambda((x: Int) => x * 2))))
//    Thread.sleep(1000)
//    executor1.send(Message(null, PoisonPill))

    system.awaitTermination()
  }

}

class Driver extends BaseActor {
  val executors: mutable.Map[String, ActorRef]          = mutable.Map.empty
  val executorCheckpoints: mutable.Map[String, Instant] = mutable.Map.empty

  val heartbeatTimeout = 5000

  override def onStart(): Unit = {
    schedule(removeNotRespondingExecutors, 1000)
  }

  override def receive(sender: ActorRef): PartialFunction[Any, Unit] = {
    case Heartbeat(id) =>
//        println(s"${getClass.getSimpleName}: executor heartbeat id=${executor.id}")
      if (executors.contains(id)) {
        executorCheckpoints.update(id, Instant.now())
      }

    case ExecutorRegister(id) =>
      val executor = sender
      val reply    = registerExecutor(executor, id)
      executor.send(Message(ref, ExecutorRegisterReply(reply)))

    case Parallelize(data) =>
      println(s"${getClass.getSimpleName}: parallelizing data...")
      val blockSize  = 10
      val blockCount = data.size / blockSize

      val partitions: Seq[Partition] =
        data
          .grouped(blockSize)
          .map(d => Partition(d))
          .toSeq

      val executorDistribution: Seq[(String, ActorRef)] =
        Seq
          .range(0, blockCount)
          .map(i => executors.toList(i % executors.size))

      val result: Seq[(Partition, (String, ActorRef))] =
        partitions
          .zip(
            executorDistribution
          )

      result.foreach {
        case (partition, (id, executor)) =>
          println(
            s"${getClass.getSimpleName}: sending partition to executor id=$id"
          )
          executor.send(Message(ref, ParallelizeParition(partition)))
      }

    case payload @ MapTask(_) =>
      executors.values.foreach(_.send(Message(ref, payload)))
  }

  private def registerExecutor(executor: ActorRef, id: String) = {
    val exists = executors.values
      .map(_.hashCode())
      .exists(_ == executor.hashCode())

    if (exists) {
      Left(s"executor already registered: hash=${executor.hashCode()}")
    } else {
      executors += (id -> executor)
      println(
        s"${getClass.getSimpleName}: add executor ${executor.hashCode()} -> $id"
      )

      Right(id)
    }
  }

  private def removeNotRespondingExecutors(): Unit =
    executorCheckpoints
      .filter {
        case (_, t) =>
          JDuration.between(Instant.now(), t).abs().toMillis > heartbeatTimeout
      }
      .foreach {
        case (id, _) =>
          executors.remove(id)
          executorCheckpoints.remove(id)
          println(s"${getClass.getSimpleName}: executor removed id=$id")
      }
}

class Executor(id: String, driver: ActorRef) extends BaseActor {

  var partitions: ArrayBuffer[Partition] = ArrayBuffer[Partition]()

  override def onStart(): Unit = {
    driver.send(Message(ref, ExecutorRegister(id)))
    schedule(() => driver.send(Message(ref, Heartbeat(id))), 1000)
  }

  override def receive(sender: ActorRef): PartialFunction[Any, Unit] = {
    case ExecutorRegisterReply(Left(m)) =>
      println(s"${getClass.getSimpleName}#${hashCode()}: from driver: $m")

    case ExecutorRegisterReply(Right(id)) =>
      println(
        s"${getClass.getSimpleName}#${hashCode()}: executor registered has id=$id"
      )

    case ParallelizeParition(partition) =>
      println(
        s"${getClass.getSimpleName}#$id: get partition: $partition"
      )
      partitions += partition

    case MapTask(lambda) =>
      val f = deserializeLambda(lambda).asInstanceOf[Int => Int]
      val result: ArrayBuffer[Partition] =
        partitions
          .map(partition => Partition(partition.data.map(f)))
      partitions = result
      println(s"${getClass.getSimpleName}#$id: map on partitions = $result")

    case PoisonPill =>
      stop()
  }

  override def onStop(): Unit =
    println(
      s"${getClass.getSimpleName}#${hashCode()}: executor stopped (id=$id)"
    )

}

case class Heartbeat(id: String)
case class ExecutorRegister(id: String)
case class ExecutorRegisterReply(idOrError: Either[String, String])
case class Parallelize(data: Seq[Int])
case class ParallelizeParition(partition: Partition)
case class Job(tasks: Seq[Task])
case object PoisonPill

case class Partition(data: Seq[Int])

sealed trait Task
case class MapTask(lambda: Array[Byte]) extends Task
