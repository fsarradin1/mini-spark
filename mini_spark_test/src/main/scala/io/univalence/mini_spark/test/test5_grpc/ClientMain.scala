package io.univalence.mini_spark.test.test5_grpc

import io.grpc.ManagedChannelBuilder
import io.univalence.mini_spark.protocol.echo.{EchoRequest, MasterGrpc}

object ClientMain {
  def main(args: Array[String]): Unit = {
    val builder =
      ManagedChannelBuilder
        .forAddress("127.0.0.1", 19092)

    builder.usePlaintext()

    val channel = builder.build()

    try {
      val client = MasterGrpc.blockingStub(channel)
      val reply  = client.processEcho(EchoRequest("hello"))
      println(reply)
    } finally {
      channel.shutdown()
    }
  }
}
