package io.univalence.mini_spark.test

package object test9_remote_actor_2 {

  case class ActorMessage(sender: ActorRef, payload: Any)

}
