package io.univalence.mini_spark.test.test9_remote_actor_2

import java.util.{Timer, TimerTask}
import java.util.concurrent.LinkedBlockingDeque
import java.util.concurrent.atomic.AtomicBoolean
import scala.annotation.tailrec
import scala.collection.mutable.ArrayBuffer
import scala.util.Random

/**
 * Base implementation for actors.
 */
trait BaseActor extends Actor {
  override private[test9_remote_actor_2] val inbox    = new LinkedBlockingDeque[ActorMessage]()
  private val isRunning: AtomicBoolean = new AtomicBoolean(false)
  private val timerTasks: ArrayBuffer[(TimerTask, Long)] =
    ArrayBuffer[(TimerTask, Long)]()
  private val timer: Timer = new Timer()

  override def start(): Unit = {
    @tailrec
    def loop(): Unit = {
      if (!isRunning.get()) ()
      else {
//        println(s"$name: waiting for message...")
        val message = inbox.take()
//        println(s"$name: got message $message")
        receive(message.sender)(message.payload)

        loop()
      }
    }

    if (isRunning.compareAndSet(false, true)) {
      onStart()
      startScheduler() // for scheduled tasks
      loop()
    }
  }

  override def stop(): Unit = {
    if (isRunning.compareAndSet(true, false)) {
      timerTasks.foreach(_._1.cancel())
      timer.purge()
      timer.cancel()
      onStop()
    }
  }

  override def kill(): Unit = System.exit(1)

  private def startScheduler(): Unit = {
//    println(s"$name: starting scheduled tasks for $timerTasks")
    timerTasks.foreach {
      case (task, period) =>
        try {
          timer.schedule(task, 1000 + Random.nextInt(500), period)
        } catch {
          case _: IllegalStateException =>
            println(s"$name: $task already scheduled")
        }
    }
//    println(s"$name: scheduled tasks started")
  }

  def schedule(task: () => Unit, period: Long): Unit = {
    val timerTask =
      new TimerTask {
        override def run(): Unit = task()
      }

    timerTasks += (timerTask -> period)
    if (isRunning.get())
      timer.schedule(timerTask, 100, period)
  }
  override def onStart(): Unit = {}
  override def onStop(): Unit  = {}

}

/**
 * Base implementation for actors that communicate only in the same
 * JVM.
 *
 * @param name unique actor name
 */
abstract class BaseLocalActor(override val name: String) extends BaseActor {
  override implicit val self: ActorRef = LocalActorRef(this, name)
}

/**
 * Base implementation for actors that has capability to communicate
 * remotely.
 *
 * @param host host of this actor
 * @param port listening port of this actor
 * @param name unique actor name
 */
abstract class BaseRemoteActor(
    val host: String,
    val port: Int,
    override val name: String
) extends BaseActor {
  override implicit val self: ActorRef = RemoteActorRef(host, port, name)

  /**
   * Local reference on this actor.
   *
   * Avoid remote communication.
   */
  val selfLocal: ActorRef              = LocalActorRef(this, name)
}
