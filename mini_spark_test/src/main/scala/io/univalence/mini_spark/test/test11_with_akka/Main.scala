package io.univalence.mini_spark.test.test11_with_akka

import akka.actor.{Actor, ActorSystem, PoisonPill, Props}
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.Random


object Main {
  def main(args: Array[String]): Unit = {
    val system = ActorSystem("actor-system")

    val ping = system.actorOf(Props(new PingPong("ping", system)))
    val pong = system.actorOf(Props(new PingPong("pong", system)))

    pong.tell(Ball, ping)

    Await.ready(system.whenTerminated, Duration.Inf)
  }
}

class PingPong(name: String, system: ActorSystem) extends Actor {
  override def receive: Receive = {
    case Ball =>
      Thread.sleep(500)
      if (Random.nextInt(100) < 10) {
        println(s"${name.toUpperCase()}: missed!!!")
        sender() ! Miss
        self ! Miss
      } else {
        println(s"${name.toUpperCase()}")
        sender() ! Ball
      }
    case Miss =>
      system.terminate()
  }
}

case object Ball
case object Miss