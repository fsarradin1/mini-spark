package io.univalence.mini_spark.test.test1_selector;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

public class Server2Main {

    public static void main(String[] args) throws Exception {
        try (ServerSocketChannel server = ServerSocketChannel.open()) {
            Selector selector = Selector.open();

            server.bind(new InetSocketAddress("0.0.0.0", 19092));
            server.configureBlocking(false);
            server.register(selector, server.validOps());

            ByteBuffer buffer = ByteBuffer.allocate(1024);

            while (true) {
                int n = selector.select();
                if (n < 0) {
                    continue;
                }
                Iterator<SelectionKey> keys = selector.selectedKeys().iterator();
                while (keys.hasNext()) {
                    SelectionKey key = keys.next();

                    if (key.isAcceptable()) {
                        SocketChannel client = server.accept();
                        client.configureBlocking(false);
                        client.register(selector, client.validOps());
                    }
                    if (key.isReadable()) {
                        SocketChannel client = (SocketChannel) key.attachment();

                        client.read(buffer);
                        String data = new String(buffer.array());
                        System.out.println("got: " + data);
                        buffer.clear();
                        buffer.put("OK".getBytes());
                        client.write(buffer);
                    }

                    keys.remove();
                }
            }
        }
    }

}
