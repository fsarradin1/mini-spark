package io.univalence.mini_spark.test.test1_selector

import java.net.InetSocketAddress
import java.nio.ByteBuffer
import java.nio.channels.{
  SelectionKey,
  Selector,
  ServerSocketChannel,
  SocketChannel
}
import java.nio.charset.StandardCharsets
import scala.util.Using

object ServerMain {
  def main(args: Array[String]): Unit = {
    Using(ServerSocketChannel.open()) { server =>
      val selector = Selector.open()

      val address = new InetSocketAddress("0.0.0.0", 19092)
      println(s"binding to ${address.getHostName}:${address.getPort}...")
      server.bind(address)
      server.configureBlocking(false)
      server.register(selector, server.validOps())

      while (true) {
        val n = selector.select()
        if (n > 0) {
          val selectionKeys = selector.selectedKeys()
          val keys          = selectionKeys.iterator()

          while (keys.hasNext) {
            val key = keys.next()
            keys.remove()

            handleKey(server, selector, key)
          }
        }
      }
    }.get
  }

  private def handleKey(
      server: ServerSocketChannel,
      selector: Selector,
      key: SelectionKey
  ): Unit = {
    if (key.isValid) {
      if (key.isAcceptable) {
        val client = server.accept()
        println(s"new client: ${client.getRemoteAddress}")
        client.configureBlocking(false)
        client.register(selector, client.validOps())
      }
      if (key.isReadable) {
        val client = key.channel().asInstanceOf[SocketChannel]

        println(s"getting data from ${client.getRemoteAddress}")
        val data: Option[String] = readRequest(client)

        data.foreach { d =>
          println(d)
          handleRequest(d, client)
        }
        if (data.isEmpty) {
          println(s"connection with ${client.getRemoteAddress} closed...")
          client.close()
        }
      }
    }
  }

  private def handleRequest(request: String, client: SocketChannel): Unit =
    if (request.toUpperCase() == "BYE") {
      println("closing...")
      client.close()
    } else {
      println(s"responding to ${client.getRemoteAddress}...")
      val response = s"OK: $request"
      sendResponse(response, client)
    }

  private def sendResponse(response: String, client: SocketChannel): Unit = {
    val raw = response.getBytes(StandardCharsets.UTF_8)

    val responseBuffer = ByteBuffer.allocate(4 + raw.length)
    responseBuffer.putInt(raw.length)
    responseBuffer.put(raw)
    responseBuffer.rewind()

    client.write(responseBuffer)
  }

  private def readRequest(client: SocketChannel): Option[String] = {
    val lengthBuffer = ByteBuffer.allocate(4)
    val readBytes    = client.read(lengthBuffer)

    if (readBytes < 0) {
      None
    } else {
      lengthBuffer.rewind()
      val length = lengthBuffer.getInt()

      val buffer = ByteBuffer.allocate(length)
      client.read(buffer)
      buffer.rewind()

      Some(new String(buffer.array()))
    }
  }
}
