package io.univalence.mini_spark

import io.univalence.mini_spark.rdd.{RDD, RemoteJobContext}

object JobMain {
  val driverHost = "127.0.0.1"
  val driverPort = 19090
  val driverName = "remote-driver"

  def main(args: Array[String]): Unit = {
//    val filepath = "data/small.csv"
    val filepath = "data/split-file/"

    val context = new RemoteJobContext(driverHost, driverPort, driverName, 2)
    context.driver.executorCountdownLatch.await()

//    val data = context.readTextFile(filepath, 5)
    val data: RDD[String] = context.readSplitTextFile(filepath)
    val shoots: RDD[Shoot] =
      data
        .filter(line => !line.startsWith("Identifiant du lieu"))
        .map { line =>
          val fields = line.split(";")

          Shoot(fields(0), fields(3), fields(4), fields(7), fields(1).toInt)
        }

    shoots.collect.foreach(println)
//    shoots.groupBy(_.year).collect.foreach(println)
    println(s"count: ${shoots.count}")

    shoots
      .map(shoot => s"${shoot.id},${shoot.title},${shoot.director},${shoot.year},${shoot.postcode}")
      .saveTextFile("data/out/out.csv")

    context.stop()
  }

}

case class Shoot(
    id: String,
    title: String,
    director: String,
    postcode: String,
    year: Int
)
