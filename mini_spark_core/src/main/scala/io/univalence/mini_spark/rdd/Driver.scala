package io.univalence.mini_spark.rdd

import actor4fun.{Actor, ActorRef, ActorSystem}
import io.univalence.mini_spark.rdd.message._
import java.time.{Instant, Duration => JDuration}
import java.util.concurrent.CountDownLatch
import org.slf4j.{Logger, LoggerFactory}
import scala.collection.mutable
import scala.concurrent.Promise

class Driver(name: String, system: ActorSystem, executorCount: Int)
    extends Actor {
  val HeartbeatTimeout: Int = 10 * 1000

  val logger: Logger = LoggerFactory.getLogger(name)

  /**
    * Registered executors
    */
  val executors: mutable.Map[String, ActorRef] = mutable.Map.empty

  /**
    * Last executor health check timestamp
    */
  val executorCheckpoints: mutable.Map[String, Instant] = mutable.Map.empty

  // hint: help to wait for a certain number of executors
  val executorCountdownLatch = new CountDownLatch(executorCount)

  var taskResults: mutable.Map[String, Promise[Any]] = mutable.Map()

  def registerTask[A](task: TaskContext[A]): Promise[RawTaskResult[A]] = {
    val promise = Promise[RawTaskResult[A]]
    taskResults.update(task.id, promise.asInstanceOf[Promise[Any]])

    promise
  }

  override def onStart(implicit self: ActorRef): Unit = {
    logger.info(s"driver started")
    system.schedule(100, 1000) { () => removeNotRespondingExecutors() }
  }

  override def receive(sender: ActorRef)(implicit self: ActorRef): Receive = {
    case Heartbeat(id) =>
      if (executors.contains(id)) {
        executorCheckpoints.update(id, Instant.now())
//        println(s"$name: executor heartbeat id=$id updated")
      } else {
        logger.warn(
          s"executor id=$id unknown on heartbeat. Ask to register again."
        )
        sender ! ExecutorRegisterAgain(id)
      }

    case ExecutorRegister(id) =>
      logger.debug(s"registering executor id=$id...")
      val reply = registerExecutor(sender, id)
      executorCountdownLatch.countDown()
      sender ! ExecutorRegisterReply(reply)

    case ExecutorTaskResult(taskId, results) =>
      logger.info(s"getting results of task $taskId from $sender")
      taskResults(taskId).success(results)
  }

  private def registerExecutor(
      executor: ActorRef,
      id: String
  ): Either[String, String] = {
    if (executors.contains(id)) {
      logger.warn(s"executor id=$id is already registered")
      Left(s"executor already registered: id=${id}")
    } else {
      executors += (id -> executor)
      logger.info(s"add executor $id -> ${executor.name}")

      Right(id)
    }
  }

  private def removeNotRespondingExecutors(): Unit = {
    val oldHeartbeats: Iterable[String] =
      executorCheckpoints.filter { case (_, t) => isOld(t) }.keys

    logger.trace(s"executors with old heartbeat: $oldHeartbeats")

    oldHeartbeats
      .foreach { id =>
        executors.remove(id)
        executorCheckpoints.remove(id)
        logger.info(s"executor removed id=$id")
      }
  }

  private def isOld(t: Instant) =
    JDuration
      .between(Instant.now(), t)
      .abs()
      .toMillis > HeartbeatTimeout
}
