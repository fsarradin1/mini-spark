package io.univalence.mini_spark.rdd

import actor4fun.{ActorRef, ActorSystem}
import io.univalence.mini_spark.rdd.RDD.{SeqRDD, SplitTextFileRDD, TextFileRDD}
import io.univalence.mini_spark.rdd.message.ExecutorTask
import java.util.UUID
import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

trait JobContext {
  def readTextFile(filepath: String): RDD[String]
  def runJob[A, B](rdd: RDD[A], postProcess: Seq[TaskResult[A]] => B): B
  def runJob[A](rdd: RDD[A]): Seq[A] =
    runJob(rdd, (x: Seq[TaskResult[A]]) => x.flatMap(_.data))
  def display[A](rdd: RDD[A]): Unit
}

class RemoteJobContext(
    host: String,
    port: Int,
    name: String,
    executorCount: Int
) extends JobContext {
  val system: ActorSystem =
    ActorSystem.createRemote(name + "-system", host, port)(
      ExecutionContext.global
    )
  sys.addShutdownHook(system.shutdown())
  val driver: Driver      = new Driver(name, system, executorCount)
  val driverRef: ActorRef = system.registerAndManage(name, driver)

  val sequence = new AtomicInteger(0)

  def parallelize[A](seq: Seq[A]): RDD[A] =
    SeqRDD(seq, driver.executors.size, this)

  def readTextFile(filepath: String): RDD[String] =
    TextFileRDD(filepath, driver.executors.size, this)

  def readTextFile(filepath: String, partitionCount: Int): RDD[String] =
    TextFileRDD(filepath, partitionCount, this)

  def readSplitTextFile(filepath: String): RDD[String] =
    SplitTextFileRDD(filepath, this)

  override def runJob[A, B](rdd: RDD[A], postProcess: Seq[TaskResult[A]] => B): B = {
    // create task
    val tasks: Seq[TaskContext[A]] = rdd.partitions.map { p =>
        TaskContext(
          id = sequence.getAndIncrement().toString,
          partition = p,
          rdd = rdd.toLocal
        )
      }

    // register tasks
    val futures: Seq[Future[RawTaskResult[A]]] =
      tasks.map { task => driver.registerTask(task).future }

    // send tasks
    tasks.zipWithIndex.foreach { case (task, idx) =>
        val (_, ref) = driver.executors.toList(idx % driver.executors.size)
        println(s"${getClass.getSimpleName}: sending $task to $ref")
        ref.sendFrom(
          driverRef,
          ExecutorTask(task.asInstanceOf[TaskContext[Any]])
        )
    }

    val result: Seq[RawTaskResult[A]] = futures.map(f => Await.result(f, Duration.Inf))
    tasks.foreach(task => driver.taskResults.remove(task.id))
    postProcess(result.map(_.toAbstract))
  }

  override def display[A](rdd: RDD[A]): Unit = {
    val tasks =
      rdd.partitions.map { p =>
        TaskContext(
          id = UUID.randomUUID().toString,
          partition = p,
          rdd = rdd.toLocal
        )
      }

    tasks.zipWithIndex.foreach {
      case (task, idx) =>
        val (_, ref) = driver.executors.toList(idx % driver.executors.size)
        println(s"${getClass.getSimpleName}: sending $task to $ref")
        ref.sendFrom(
          driverRef,
          ExecutorTask(task.asInstanceOf[TaskContext[Any]])
        )
    }
  }

  def awaitTermination(): Unit = system.awaitTermination()

  def stop(): Unit = system.shutdown()
}
object JobContext {
  def create(
      host: String,
      port: Int,
      name: String,
      executorCount: Int
  ): JobContext =
    new RemoteJobContext(host, port, name, executorCount)

//  class JobActor() extends Actor {
//    override def receive(
//        sender: ActorRef
//    )(implicit self: ActorRef): PartialFunction[Any, Unit] = {
//      case PoisonPill => stop(); kill()
//      case _          => ()
//    }
//  }

}

class LocalJobContext() extends JobContext with Serializable {
  def readTextFile(filepath: String): RDD[String] =
    TextFileRDD(filepath, 2, this)

  def runJob[A, B](rdd: RDD[A], postProcess: Seq[TaskResult[A]] => B): B = {
    val tasks: Seq[TaskContext[A]] =
      rdd.partitions.map { p =>
        TaskContext(
          id = UUID.randomUUID().toString,
          partition = p,
          rdd = rdd
        )
      }

    val result =
      tasks.map { t =>
        val results = t.rdd.compute(t.partition)

        TaskResult(
          id = t.id,
          context = t,
          data = results.toSeq
        )
      }

    postProcess(result)
  }

  override def display[A](rdd: RDD[A]): Unit = {
    runJob(rdd).foreach(println)
  }
}

object LocalJobContext {
  def create: LocalJobContext = new LocalJobContext()
}
