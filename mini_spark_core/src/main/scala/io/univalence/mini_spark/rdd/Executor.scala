package io.univalence.mini_spark.rdd

import actor4fun.{Actor, ActorRef, ActorSystem}
import io.univalence.mini_spark.rdd.message._
import org.slf4j.{Logger, LoggerFactory}
import scala.util.Using
import scala.util.control.NonFatal

class Executor(id: String, driver: ActorRef, system: ActorSystem)
    extends Actor {

  val HeartbeatInterval: Int = 5 * 1000
  val HeartbeatMaxRetry: Int = 3

  val name = s"executor-$id"
  val logger: Logger = LoggerFactory.getLogger(name)

  var heartbeatRetry: Int         = 0
  var heartbeatScheduled: Boolean = false

  def heartbeatSender(implicit self: ActorRef): Unit =
    try {
      driver ! Heartbeat(id)
      heartbeatRetry = 0
    } catch {
      case NonFatal(e) =>
        heartbeatRetry += 1
        if (heartbeatRetry < HeartbeatMaxRetry) {
          logger.warn(
            s"""driver ${driver.name} is not responding due to "${e.getMessage}". Then retry #$heartbeatRetry."""
          )
        } else {
          logger.warn(
            s"""driver ${driver.name} is not responding due to "${e.getMessage}". Then shutdown executor."""
          )
          self ! PoisonPill
        }
    }

  override def onStart(implicit self: ActorRef): Unit = {
    logger.info(s"registering to driver ${driver.name}")
    driver ! ExecutorRegister(id)
  }

  override def receive(sender: ActorRef)(implicit self: ActorRef): Receive = {
    case ExecutorRegisterReply(Left(errorMessage)) =>
      logger.error(s"driver get error while registering: $errorMessage")

    case ExecutorRegisterReply(Right(id)) =>
      logger.info(s"executor registered has id=$id")
      if (!heartbeatScheduled) {
        system.schedule(100, HeartbeatInterval) { () => heartbeatSender }
        heartbeatScheduled = true
      }

    case ExecutorRegisterAgain(_) =>
      logger.warn(s"driver ${driver.name} asked to register again")
      driver ! ExecutorRegister(id)

    case ExecutorTask(task) =>
      logger.info(s"received ${task.id} from $sender")
      val result = Using(task.rdd.compute(task.partition)) { _.toArray }.get
      logger.info(s"send results of ${task.id} to $sender")
      sender ! ExecutorTaskResult(task.id, RawTaskResult(task.id, task, result))

    case PoisonPill =>
      system.shutdown()
  }

  override def onShutdown(): Unit =
    logger.info(s"executor stopped (id=$id)")

}
