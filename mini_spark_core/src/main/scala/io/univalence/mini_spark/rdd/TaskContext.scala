package io.univalence.mini_spark.rdd

case class TaskContext[A](
    id: String,
    partition: Partition,
    rdd: RDD[A]
)

case class RawTaskResult[A](
    id: String,
    context: TaskContext[A],
    data: Array[A]
) {
  def toAbstract: TaskResult[A] =
    TaskResult(id, context, data.toSeq)
}

case class TaskResult[A](
    id: String,
    context: TaskContext[A],
    data: Seq[A]
)
