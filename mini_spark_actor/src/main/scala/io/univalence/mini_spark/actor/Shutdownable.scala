package io.univalence.mini_spark.actor

trait Shutdownable {
  def shutdown(): Unit
}
