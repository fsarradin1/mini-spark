package io.univalence.mini_spark.actor

case class RemoteActorSystemRef(host: String, port: Int)
