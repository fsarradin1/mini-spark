package io.univalence.mini_spark.actor.example

import io.univalence.mini_spark.actor.{Actor, ActorRef, ActorSystem, LocalActorRef}

object HelloMain {

  def main(args: Array[String]): Unit = {
    import scala.concurrent.ExecutionContext.Implicits._

    val system = ActorSystem.createLocal("actor-system")
    sys.addShutdownHook(system.shutdown())

    val hello = system.registerAndManage("hello", new Hello)

    hello.sendFrom(hello, "John")

    system.awaitTermination()
  }

}

class Hello extends Actor {
  override def receive(sender: ActorRef)(implicit self: ActorRef): Receive =
    message => println(s"Hello $message!")
}
