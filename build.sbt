import sbt.Keys.version

lazy val libVersion = new {
  val scala = "2.13.13"
}

lazy val root =
  (project in file("."))
    .settings(
      name := "mini_spark",
      version := "0.1",
      scalaVersion := libVersion.scala
    )
    .aggregate(mini_spark_actor, mini_spark_core, mini_spark_test)

lazy val mini_spark_core =
  (project in file("mini_spark_core"))
    .settings(
      name := "mini_spark_core",
      version := "0.1",
      scalaVersion := libVersion.scala,
      libraryDependencies ++= Seq(
        "io.univalence" %% "actor4fun" % "0.0.2"
      ) ++ commonDeps
    )

lazy val mini_spark_actor =
  (project in file("mini_spark_actor"))
    .settings(
      name := "mini_spark_actor",
      version := "0.1",
      scalaVersion := libVersion.scala,
      libraryDependencies ++= Seq(
        "io.grpc"               % "grpc-netty"           % scalapb.compiler.Version.grpcJavaVersion,
        "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapb.compiler.Version.scalapbVersion,
        "com.thesamet.scalapb" %% "scalapb-runtime"      % scalapb.compiler.Version.scalapbVersion % "protobuf"
      ) ++ commonDeps,
      Compile / PB.targets := Seq(
        scalapb.gen() -> (Compile / sourceManaged).value / "scalapb"
      )
    )

lazy val mini_spark_test =
  (project in file("mini_spark_test"))
    .settings(
      name := "mini_spark_test",
      version := "0.1",
      scalaVersion := libVersion.scala,
      libraryDependencies ++= Seq(
        "org.codehaus.janino"   % "janino"               % "3.1.2",
        "com.propensive"       %% "magnolia"             % "0.17.0",
        "io.grpc"               % "grpc-netty"           % scalapb.compiler.Version.grpcJavaVersion,
        "com.thesamet.scalapb" %% "scalapb-runtime-grpc" % scalapb.compiler.Version.scalapbVersion,
        "com.thesamet.scalapb" %% "scalapb-runtime"      % scalapb.compiler.Version.scalapbVersion % "protobuf",
        "org.scala-lang"        % "scala-reflect"        % scalaVersion.value                      % Provided,

        "io.univalence" %% "actor4fun" % "0.0.2",

        "dev.zio" %% "zio-actors" % "0.0.7",

        "com.typesafe.akka" %% "akka-actor-typed" % "2.6.10"
      ) ++ commonDeps,
      Compile / PB.targets := Seq(
        scalapb.gen() -> (Compile / sourceManaged).value / "scalapb"
      )
    )
    .dependsOn(mini_spark_core)

val commonDeps: Seq[ModuleID] = Seq(
  "ch.qos.logback"     % "logback-classic" % "1.2.3",
  "ch.qos.logback"     % "logback-core"    % "1.2.3",
  "org.slf4j"          % "slf4j-api"       % "1.7.30",
  "org.scalatest"     %% "scalatest"       % "3.2.2"   % Test,
  "org.scalacheck"    %% "scalacheck"      % "1.14.1"  % Test,
  "org.scalatestplus" %% "scalacheck-1-14" % "3.2.2.0" % Test
)
