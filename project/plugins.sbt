addSbtPlugin("com.thesamet"   % "sbt-protoc"   % "1.0.7")
addSbtPlugin("org.scalameta"  % "sbt-scalafmt" % "2.5.2")

libraryDependencies += "com.thesamet.scalapb" %% "compilerplugin" % "0.10.8"